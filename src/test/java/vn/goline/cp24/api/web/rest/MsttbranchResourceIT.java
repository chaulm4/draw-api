package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.Msttbranch;
import vn.goline.cp24.api.repository.MsttbranchRepository;
import vn.goline.cp24.api.service.dto.MsttbranchDTO;
import vn.goline.cp24.api.service.mapper.MsttbranchMapper;

/**
 * Integration tests for the {@link MsttbranchResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MsttbranchResourceIT {

    private static final String DEFAULT_BRANCH_CD = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/msttbranches";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MsttbranchRepository msttbranchRepository;

    @Autowired
    private MsttbranchMapper msttbranchMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMsttbranchMockMvc;

    private Msttbranch msttbranch;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttbranch createEntity(EntityManager em) {
        Msttbranch msttbranch = new Msttbranch()
            .branchCd(DEFAULT_BRANCH_CD)
            .branchName(DEFAULT_BRANCH_NAME)
            .remarks(DEFAULT_REMARKS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return msttbranch;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttbranch createUpdatedEntity(EntityManager em) {
        Msttbranch msttbranch = new Msttbranch()
            .branchCd(UPDATED_BRANCH_CD)
            .branchName(UPDATED_BRANCH_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return msttbranch;
    }

    @BeforeEach
    public void initTest() {
        msttbranch = createEntity(em);
    }

    @Test
    @Transactional
    void createMsttbranch() throws Exception {
        int databaseSizeBeforeCreate = msttbranchRepository.findAll().size();
        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);
        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isCreated());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeCreate + 1);
        Msttbranch testMsttbranch = msttbranchList.get(msttbranchList.size() - 1);
        assertThat(testMsttbranch.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttbranch.getBranchName()).isEqualTo(DEFAULT_BRANCH_NAME);
        assertThat(testMsttbranch.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttbranch.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttbranch.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttbranch.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttbranch.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createMsttbranchWithExistingId() throws Exception {
        // Create the Msttbranch with an existing ID
        msttbranch.setId(1L);
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        int databaseSizeBeforeCreate = msttbranchRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBranchCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setBranchCd(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBranchNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setBranchName(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRemarksIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setRemarks(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setRegDateTime(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setRegUserId(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setUpdDateTime(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbranchRepository.findAll().size();
        // set the field null
        msttbranch.setUpdUserId(null);

        // Create the Msttbranch, which fails.
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        restMsttbranchMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMsttbranches() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        // Get all the msttbranchList
        restMsttbranchMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msttbranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].branchCd").value(hasItem(DEFAULT_BRANCH_CD)))
            .andExpect(jsonPath("$.[*].branchName").value(hasItem(DEFAULT_BRANCH_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getMsttbranch() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        // Get the msttbranch
        restMsttbranchMockMvc
            .perform(get(ENTITY_API_URL_ID, msttbranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(msttbranch.getId().intValue()))
            .andExpect(jsonPath("$.branchCd").value(DEFAULT_BRANCH_CD))
            .andExpect(jsonPath("$.branchName").value(DEFAULT_BRANCH_NAME))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingMsttbranch() throws Exception {
        // Get the msttbranch
        restMsttbranchMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMsttbranch() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();

        // Update the msttbranch
        Msttbranch updatedMsttbranch = msttbranchRepository.findById(msttbranch.getId()).get();
        // Disconnect from session so that the updates on updatedMsttbranch are not directly saved in db
        em.detach(updatedMsttbranch);
        updatedMsttbranch
            .branchCd(UPDATED_BRANCH_CD)
            .branchName(UPDATED_BRANCH_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(updatedMsttbranch);

        restMsttbranchMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttbranchDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isOk());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
        Msttbranch testMsttbranch = msttbranchList.get(msttbranchList.size() - 1);
        assertThat(testMsttbranch.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttbranch.getBranchName()).isEqualTo(UPDATED_BRANCH_NAME);
        assertThat(testMsttbranch.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbranch.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbranch.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttbranch.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttbranch.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttbranchDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbranchDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMsttbranchWithPatch() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();

        // Update the msttbranch using partial update
        Msttbranch partialUpdatedMsttbranch = new Msttbranch();
        partialUpdatedMsttbranch.setId(msttbranch.getId());

        partialUpdatedMsttbranch
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttbranchMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttbranch.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttbranch))
            )
            .andExpect(status().isOk());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
        Msttbranch testMsttbranch = msttbranchList.get(msttbranchList.size() - 1);
        assertThat(testMsttbranch.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttbranch.getBranchName()).isEqualTo(DEFAULT_BRANCH_NAME);
        assertThat(testMsttbranch.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbranch.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbranch.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttbranch.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttbranch.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMsttbranchWithPatch() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();

        // Update the msttbranch using partial update
        Msttbranch partialUpdatedMsttbranch = new Msttbranch();
        partialUpdatedMsttbranch.setId(msttbranch.getId());

        partialUpdatedMsttbranch
            .branchCd(UPDATED_BRANCH_CD)
            .branchName(UPDATED_BRANCH_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttbranchMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttbranch.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttbranch))
            )
            .andExpect(status().isOk());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
        Msttbranch testMsttbranch = msttbranchList.get(msttbranchList.size() - 1);
        assertThat(testMsttbranch.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttbranch.getBranchName()).isEqualTo(UPDATED_BRANCH_NAME);
        assertThat(testMsttbranch.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbranch.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbranch.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttbranch.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttbranch.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, msttbranchDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMsttbranch() throws Exception {
        int databaseSizeBeforeUpdate = msttbranchRepository.findAll().size();
        msttbranch.setId(count.incrementAndGet());

        // Create the Msttbranch
        MsttbranchDTO msttbranchDTO = msttbranchMapper.toDto(msttbranch);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbranchMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(msttbranchDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttbranch in the database
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMsttbranch() throws Exception {
        // Initialize the database
        msttbranchRepository.saveAndFlush(msttbranch);

        int databaseSizeBeforeDelete = msttbranchRepository.findAll().size();

        // Delete the msttbranch
        restMsttbranchMockMvc
            .perform(delete(ENTITY_API_URL_ID, msttbranch.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Msttbranch> msttbranchList = msttbranchRepository.findAll();
        assertThat(msttbranchList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
