package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.Msttbroker;
import vn.goline.cp24.api.repository.MsttbrokerRepository;
import vn.goline.cp24.api.service.dto.MsttbrokerDTO;
import vn.goline.cp24.api.service.mapper.MsttbrokerMapper;

/**
 * Integration tests for the {@link MsttbrokerResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MsttbrokerResourceIT {

    private static final String DEFAULT_BROKER_CD = "AAAAAAAAAA";
    private static final String UPDATED_BROKER_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BROKER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BROKER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_POSITION = "AAAAAAAAAA";
    private static final String UPDATED_POSITION = "BBBBBBBBBB";

    private static final String DEFAULT_MANAGEMENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MANAGEMENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT_CD = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH_CD = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_CD = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/msttbrokers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MsttbrokerRepository msttbrokerRepository;

    @Autowired
    private MsttbrokerMapper msttbrokerMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMsttbrokerMockMvc;

    private Msttbroker msttbroker;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttbroker createEntity(EntityManager em) {
        Msttbroker msttbroker = new Msttbroker()
            .brokerCd(DEFAULT_BROKER_CD)
            .brokerName(DEFAULT_BROKER_NAME)
            .position(DEFAULT_POSITION)
            .managementId(DEFAULT_MANAGEMENT_ID)
            .departmentCd(DEFAULT_DEPARTMENT_CD)
            .branchCd(DEFAULT_BRANCH_CD)
            .remarks(DEFAULT_REMARKS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return msttbroker;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttbroker createUpdatedEntity(EntityManager em) {
        Msttbroker msttbroker = new Msttbroker()
            .brokerCd(UPDATED_BROKER_CD)
            .brokerName(UPDATED_BROKER_NAME)
            .position(UPDATED_POSITION)
            .managementId(UPDATED_MANAGEMENT_ID)
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return msttbroker;
    }

    @BeforeEach
    public void initTest() {
        msttbroker = createEntity(em);
    }

    @Test
    @Transactional
    void createMsttbroker() throws Exception {
        int databaseSizeBeforeCreate = msttbrokerRepository.findAll().size();
        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);
        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isCreated());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeCreate + 1);
        Msttbroker testMsttbroker = msttbrokerList.get(msttbrokerList.size() - 1);
        assertThat(testMsttbroker.getBrokerCd()).isEqualTo(DEFAULT_BROKER_CD);
        assertThat(testMsttbroker.getBrokerName()).isEqualTo(DEFAULT_BROKER_NAME);
        assertThat(testMsttbroker.getPosition()).isEqualTo(DEFAULT_POSITION);
        assertThat(testMsttbroker.getManagementId()).isEqualTo(DEFAULT_MANAGEMENT_ID);
        assertThat(testMsttbroker.getDepartmentCd()).isEqualTo(DEFAULT_DEPARTMENT_CD);
        assertThat(testMsttbroker.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttbroker.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttbroker.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttbroker.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttbroker.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttbroker.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createMsttbrokerWithExistingId() throws Exception {
        // Create the Msttbroker with an existing ID
        msttbroker.setId(1L);
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        int databaseSizeBeforeCreate = msttbrokerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBrokerCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setBrokerCd(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBrokerNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setBrokerName(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRemarksIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setRemarks(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setRegDateTime(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setRegUserId(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setUpdDateTime(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttbrokerRepository.findAll().size();
        // set the field null
        msttbroker.setUpdUserId(null);

        // Create the Msttbroker, which fails.
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        restMsttbrokerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isBadRequest());

        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMsttbrokers() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        // Get all the msttbrokerList
        restMsttbrokerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msttbroker.getId().intValue())))
            .andExpect(jsonPath("$.[*].brokerCd").value(hasItem(DEFAULT_BROKER_CD)))
            .andExpect(jsonPath("$.[*].brokerName").value(hasItem(DEFAULT_BROKER_NAME)))
            .andExpect(jsonPath("$.[*].position").value(hasItem(DEFAULT_POSITION)))
            .andExpect(jsonPath("$.[*].managementId").value(hasItem(DEFAULT_MANAGEMENT_ID)))
            .andExpect(jsonPath("$.[*].departmentCd").value(hasItem(DEFAULT_DEPARTMENT_CD)))
            .andExpect(jsonPath("$.[*].branchCd").value(hasItem(DEFAULT_BRANCH_CD)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getMsttbroker() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        // Get the msttbroker
        restMsttbrokerMockMvc
            .perform(get(ENTITY_API_URL_ID, msttbroker.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(msttbroker.getId().intValue()))
            .andExpect(jsonPath("$.brokerCd").value(DEFAULT_BROKER_CD))
            .andExpect(jsonPath("$.brokerName").value(DEFAULT_BROKER_NAME))
            .andExpect(jsonPath("$.position").value(DEFAULT_POSITION))
            .andExpect(jsonPath("$.managementId").value(DEFAULT_MANAGEMENT_ID))
            .andExpect(jsonPath("$.departmentCd").value(DEFAULT_DEPARTMENT_CD))
            .andExpect(jsonPath("$.branchCd").value(DEFAULT_BRANCH_CD))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingMsttbroker() throws Exception {
        // Get the msttbroker
        restMsttbrokerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMsttbroker() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();

        // Update the msttbroker
        Msttbroker updatedMsttbroker = msttbrokerRepository.findById(msttbroker.getId()).get();
        // Disconnect from session so that the updates on updatedMsttbroker are not directly saved in db
        em.detach(updatedMsttbroker);
        updatedMsttbroker
            .brokerCd(UPDATED_BROKER_CD)
            .brokerName(UPDATED_BROKER_NAME)
            .position(UPDATED_POSITION)
            .managementId(UPDATED_MANAGEMENT_ID)
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(updatedMsttbroker);

        restMsttbrokerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttbrokerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
        Msttbroker testMsttbroker = msttbrokerList.get(msttbrokerList.size() - 1);
        assertThat(testMsttbroker.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttbroker.getBrokerName()).isEqualTo(UPDATED_BROKER_NAME);
        assertThat(testMsttbroker.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testMsttbroker.getManagementId()).isEqualTo(UPDATED_MANAGEMENT_ID);
        assertThat(testMsttbroker.getDepartmentCd()).isEqualTo(UPDATED_DEPARTMENT_CD);
        assertThat(testMsttbroker.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttbroker.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbroker.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbroker.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttbroker.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttbroker.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttbrokerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMsttbrokerWithPatch() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();

        // Update the msttbroker using partial update
        Msttbroker partialUpdatedMsttbroker = new Msttbroker();
        partialUpdatedMsttbroker.setId(msttbroker.getId());

        partialUpdatedMsttbroker
            .brokerCd(UPDATED_BROKER_CD)
            .position(UPDATED_POSITION)
            .managementId(UPDATED_MANAGEMENT_ID)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttbrokerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttbroker.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttbroker))
            )
            .andExpect(status().isOk());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
        Msttbroker testMsttbroker = msttbrokerList.get(msttbrokerList.size() - 1);
        assertThat(testMsttbroker.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttbroker.getBrokerName()).isEqualTo(DEFAULT_BROKER_NAME);
        assertThat(testMsttbroker.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testMsttbroker.getManagementId()).isEqualTo(UPDATED_MANAGEMENT_ID);
        assertThat(testMsttbroker.getDepartmentCd()).isEqualTo(DEFAULT_DEPARTMENT_CD);
        assertThat(testMsttbroker.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttbroker.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbroker.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbroker.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttbroker.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttbroker.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMsttbrokerWithPatch() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();

        // Update the msttbroker using partial update
        Msttbroker partialUpdatedMsttbroker = new Msttbroker();
        partialUpdatedMsttbroker.setId(msttbroker.getId());

        partialUpdatedMsttbroker
            .brokerCd(UPDATED_BROKER_CD)
            .brokerName(UPDATED_BROKER_NAME)
            .position(UPDATED_POSITION)
            .managementId(UPDATED_MANAGEMENT_ID)
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttbrokerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttbroker.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttbroker))
            )
            .andExpect(status().isOk());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
        Msttbroker testMsttbroker = msttbrokerList.get(msttbrokerList.size() - 1);
        assertThat(testMsttbroker.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttbroker.getBrokerName()).isEqualTo(UPDATED_BROKER_NAME);
        assertThat(testMsttbroker.getPosition()).isEqualTo(UPDATED_POSITION);
        assertThat(testMsttbroker.getManagementId()).isEqualTo(UPDATED_MANAGEMENT_ID);
        assertThat(testMsttbroker.getDepartmentCd()).isEqualTo(UPDATED_DEPARTMENT_CD);
        assertThat(testMsttbroker.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttbroker.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttbroker.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttbroker.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttbroker.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttbroker.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, msttbrokerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMsttbroker() throws Exception {
        int databaseSizeBeforeUpdate = msttbrokerRepository.findAll().size();
        msttbroker.setId(count.incrementAndGet());

        // Create the Msttbroker
        MsttbrokerDTO msttbrokerDTO = msttbrokerMapper.toDto(msttbroker);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttbrokerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(msttbrokerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttbroker in the database
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMsttbroker() throws Exception {
        // Initialize the database
        msttbrokerRepository.saveAndFlush(msttbroker);

        int databaseSizeBeforeDelete = msttbrokerRepository.findAll().size();

        // Delete the msttbroker
        restMsttbrokerMockMvc
            .perform(delete(ENTITY_API_URL_ID, msttbroker.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Msttbroker> msttbrokerList = msttbrokerRepository.findAll();
        assertThat(msttbrokerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
