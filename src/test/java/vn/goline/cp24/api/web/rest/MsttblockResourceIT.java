package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.Msttblock;
import vn.goline.cp24.api.repository.MsttblockRepository;
import vn.goline.cp24.api.service.dto.MsttblockDTO;
import vn.goline.cp24.api.service.mapper.MsttblockMapper;

/**
 * Integration tests for the {@link MsttblockResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MsttblockResourceIT {

    private static final String DEFAULT_BLOCK_CD = "AAAAAAAAAA";
    private static final String UPDATED_BLOCK_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BLOCK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BLOCK_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/msttblocks";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MsttblockRepository msttblockRepository;

    @Autowired
    private MsttblockMapper msttblockMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMsttblockMockMvc;

    private Msttblock msttblock;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttblock createEntity(EntityManager em) {
        Msttblock msttblock = new Msttblock()
            .blockCd(DEFAULT_BLOCK_CD)
            .blockName(DEFAULT_BLOCK_NAME)
            .remarks(DEFAULT_REMARKS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return msttblock;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttblock createUpdatedEntity(EntityManager em) {
        Msttblock msttblock = new Msttblock()
            .blockCd(UPDATED_BLOCK_CD)
            .blockName(UPDATED_BLOCK_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return msttblock;
    }

    @BeforeEach
    public void initTest() {
        msttblock = createEntity(em);
    }

    @Test
    @Transactional
    void createMsttblock() throws Exception {
        int databaseSizeBeforeCreate = msttblockRepository.findAll().size();
        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);
        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isCreated());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeCreate + 1);
        Msttblock testMsttblock = msttblockList.get(msttblockList.size() - 1);
        assertThat(testMsttblock.getBlockCd()).isEqualTo(DEFAULT_BLOCK_CD);
        assertThat(testMsttblock.getBlockName()).isEqualTo(DEFAULT_BLOCK_NAME);
        assertThat(testMsttblock.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttblock.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttblock.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttblock.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttblock.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createMsttblockWithExistingId() throws Exception {
        // Create the Msttblock with an existing ID
        msttblock.setId(1L);
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        int databaseSizeBeforeCreate = msttblockRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBlockCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setBlockCd(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBlockNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setBlockName(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRemarksIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setRemarks(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setRegDateTime(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setRegUserId(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setUpdDateTime(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockRepository.findAll().size();
        // set the field null
        msttblock.setUpdUserId(null);

        // Create the Msttblock, which fails.
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        restMsttblockMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isBadRequest());

        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMsttblocks() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        // Get all the msttblockList
        restMsttblockMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msttblock.getId().intValue())))
            .andExpect(jsonPath("$.[*].blockCd").value(hasItem(DEFAULT_BLOCK_CD)))
            .andExpect(jsonPath("$.[*].blockName").value(hasItem(DEFAULT_BLOCK_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getMsttblock() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        // Get the msttblock
        restMsttblockMockMvc
            .perform(get(ENTITY_API_URL_ID, msttblock.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(msttblock.getId().intValue()))
            .andExpect(jsonPath("$.blockCd").value(DEFAULT_BLOCK_CD))
            .andExpect(jsonPath("$.blockName").value(DEFAULT_BLOCK_NAME))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingMsttblock() throws Exception {
        // Get the msttblock
        restMsttblockMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMsttblock() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();

        // Update the msttblock
        Msttblock updatedMsttblock = msttblockRepository.findById(msttblock.getId()).get();
        // Disconnect from session so that the updates on updatedMsttblock are not directly saved in db
        em.detach(updatedMsttblock);
        updatedMsttblock
            .blockCd(UPDATED_BLOCK_CD)
            .blockName(UPDATED_BLOCK_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(updatedMsttblock);

        restMsttblockMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttblockDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isOk());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
        Msttblock testMsttblock = msttblockList.get(msttblockList.size() - 1);
        assertThat(testMsttblock.getBlockCd()).isEqualTo(UPDATED_BLOCK_CD);
        assertThat(testMsttblock.getBlockName()).isEqualTo(UPDATED_BLOCK_NAME);
        assertThat(testMsttblock.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttblock.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblock.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttblock.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttblock.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttblockDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMsttblockWithPatch() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();

        // Update the msttblock using partial update
        Msttblock partialUpdatedMsttblock = new Msttblock();
        partialUpdatedMsttblock.setId(msttblock.getId());

        partialUpdatedMsttblock.blockName(UPDATED_BLOCK_NAME).regDateTime(UPDATED_REG_DATE_TIME).updUserId(UPDATED_UPD_USER_ID);

        restMsttblockMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttblock.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttblock))
            )
            .andExpect(status().isOk());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
        Msttblock testMsttblock = msttblockList.get(msttblockList.size() - 1);
        assertThat(testMsttblock.getBlockCd()).isEqualTo(DEFAULT_BLOCK_CD);
        assertThat(testMsttblock.getBlockName()).isEqualTo(UPDATED_BLOCK_NAME);
        assertThat(testMsttblock.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttblock.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblock.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttblock.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttblock.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMsttblockWithPatch() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();

        // Update the msttblock using partial update
        Msttblock partialUpdatedMsttblock = new Msttblock();
        partialUpdatedMsttblock.setId(msttblock.getId());

        partialUpdatedMsttblock
            .blockCd(UPDATED_BLOCK_CD)
            .blockName(UPDATED_BLOCK_NAME)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttblockMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttblock.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttblock))
            )
            .andExpect(status().isOk());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
        Msttblock testMsttblock = msttblockList.get(msttblockList.size() - 1);
        assertThat(testMsttblock.getBlockCd()).isEqualTo(UPDATED_BLOCK_CD);
        assertThat(testMsttblock.getBlockName()).isEqualTo(UPDATED_BLOCK_NAME);
        assertThat(testMsttblock.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttblock.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblock.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttblock.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttblock.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, msttblockDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMsttblock() throws Exception {
        int databaseSizeBeforeUpdate = msttblockRepository.findAll().size();
        msttblock.setId(count.incrementAndGet());

        // Create the Msttblock
        MsttblockDTO msttblockDTO = msttblockMapper.toDto(msttblock);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(msttblockDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttblock in the database
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMsttblock() throws Exception {
        // Initialize the database
        msttblockRepository.saveAndFlush(msttblock);

        int databaseSizeBeforeDelete = msttblockRepository.findAll().size();

        // Delete the msttblock
        restMsttblockMockMvc
            .perform(delete(ENTITY_API_URL_ID, msttblock.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Msttblock> msttblockList = msttblockRepository.findAll();
        assertThat(msttblockList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
