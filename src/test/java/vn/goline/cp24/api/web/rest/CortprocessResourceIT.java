package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.Cortprocess;
import vn.goline.cp24.api.repository.CortprocessRepository;
import vn.goline.cp24.api.service.dto.CortprocessDTO;
import vn.goline.cp24.api.service.mapper.CortprocessMapper;

/**
 * Integration tests for the {@link CortprocessResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CortprocessResourceIT {

    private static final String DEFAULT_PROCESS_CD = "AAAAAAAAAA";
    private static final String UPDATED_PROCESS_CD = "BBBBBBBBBB";

    private static final String DEFAULT_REF_PROCESS_CD = "AAAAAAAAAA";
    private static final String UPDATED_REF_PROCESS_CD = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_ID = "AAAAAAAAAA";
    private static final String UPDATED_LAST_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_ALLOW_FLAG = 1;
    private static final Integer UPDATED_ALLOW_FLAG = 2;

    private static final Integer DEFAULT_COUNTER = 1;
    private static final Integer UPDATED_COUNTER = 2;

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/cortprocesses";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CortprocessRepository cortprocessRepository;

    @Autowired
    private CortprocessMapper cortprocessMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCortprocessMockMvc;

    private Cortprocess cortprocess;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cortprocess createEntity(EntityManager em) {
        Cortprocess cortprocess = new Cortprocess()
            .processCd(DEFAULT_PROCESS_CD)
            .refProcessCd(DEFAULT_REF_PROCESS_CD)
            .type(DEFAULT_TYPE)
            .name(DEFAULT_NAME)
            .lastTime(DEFAULT_LAST_TIME)
            .lastId(DEFAULT_LAST_ID)
            .allowFlag(DEFAULT_ALLOW_FLAG)
            .counter(DEFAULT_COUNTER)
            .status(DEFAULT_STATUS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return cortprocess;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cortprocess createUpdatedEntity(EntityManager em) {
        Cortprocess cortprocess = new Cortprocess()
            .processCd(UPDATED_PROCESS_CD)
            .refProcessCd(UPDATED_REF_PROCESS_CD)
            .type(UPDATED_TYPE)
            .name(UPDATED_NAME)
            .lastTime(UPDATED_LAST_TIME)
            .lastId(UPDATED_LAST_ID)
            .allowFlag(UPDATED_ALLOW_FLAG)
            .counter(UPDATED_COUNTER)
            .status(UPDATED_STATUS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return cortprocess;
    }

    @BeforeEach
    public void initTest() {
        cortprocess = createEntity(em);
    }

    @Test
    @Transactional
    void createCortprocess() throws Exception {
        int databaseSizeBeforeCreate = cortprocessRepository.findAll().size();
        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);
        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeCreate + 1);
        Cortprocess testCortprocess = cortprocessList.get(cortprocessList.size() - 1);
        assertThat(testCortprocess.getProcessCd()).isEqualTo(DEFAULT_PROCESS_CD);
        assertThat(testCortprocess.getRefProcessCd()).isEqualTo(DEFAULT_REF_PROCESS_CD);
        assertThat(testCortprocess.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testCortprocess.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCortprocess.getLastTime()).isEqualTo(DEFAULT_LAST_TIME);
        assertThat(testCortprocess.getLastId()).isEqualTo(DEFAULT_LAST_ID);
        assertThat(testCortprocess.getAllowFlag()).isEqualTo(DEFAULT_ALLOW_FLAG);
        assertThat(testCortprocess.getCounter()).isEqualTo(DEFAULT_COUNTER);
        assertThat(testCortprocess.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCortprocess.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testCortprocess.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testCortprocess.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testCortprocess.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createCortprocessWithExistingId() throws Exception {
        // Create the Cortprocess with an existing ID
        cortprocess.setId(1L);
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        int databaseSizeBeforeCreate = cortprocessRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkProcessCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setProcessCd(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setType(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setStatus(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setRegDateTime(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setRegUserId(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setUpdDateTime(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = cortprocessRepository.findAll().size();
        // set the field null
        cortprocess.setUpdUserId(null);

        // Create the Cortprocess, which fails.
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        restCortprocessMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCortprocesses() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        // Get all the cortprocessList
        restCortprocessMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cortprocess.getId().intValue())))
            .andExpect(jsonPath("$.[*].processCd").value(hasItem(DEFAULT_PROCESS_CD)))
            .andExpect(jsonPath("$.[*].refProcessCd").value(hasItem(DEFAULT_REF_PROCESS_CD)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].lastTime").value(hasItem(DEFAULT_LAST_TIME.toString())))
            .andExpect(jsonPath("$.[*].lastId").value(hasItem(DEFAULT_LAST_ID)))
            .andExpect(jsonPath("$.[*].allowFlag").value(hasItem(DEFAULT_ALLOW_FLAG)))
            .andExpect(jsonPath("$.[*].counter").value(hasItem(DEFAULT_COUNTER)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getCortprocess() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        // Get the cortprocess
        restCortprocessMockMvc
            .perform(get(ENTITY_API_URL_ID, cortprocess.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cortprocess.getId().intValue()))
            .andExpect(jsonPath("$.processCd").value(DEFAULT_PROCESS_CD))
            .andExpect(jsonPath("$.refProcessCd").value(DEFAULT_REF_PROCESS_CD))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.lastTime").value(DEFAULT_LAST_TIME.toString()))
            .andExpect(jsonPath("$.lastId").value(DEFAULT_LAST_ID))
            .andExpect(jsonPath("$.allowFlag").value(DEFAULT_ALLOW_FLAG))
            .andExpect(jsonPath("$.counter").value(DEFAULT_COUNTER))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingCortprocess() throws Exception {
        // Get the cortprocess
        restCortprocessMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCortprocess() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();

        // Update the cortprocess
        Cortprocess updatedCortprocess = cortprocessRepository.findById(cortprocess.getId()).get();
        // Disconnect from session so that the updates on updatedCortprocess are not directly saved in db
        em.detach(updatedCortprocess);
        updatedCortprocess
            .processCd(UPDATED_PROCESS_CD)
            .refProcessCd(UPDATED_REF_PROCESS_CD)
            .type(UPDATED_TYPE)
            .name(UPDATED_NAME)
            .lastTime(UPDATED_LAST_TIME)
            .lastId(UPDATED_LAST_ID)
            .allowFlag(UPDATED_ALLOW_FLAG)
            .counter(UPDATED_COUNTER)
            .status(UPDATED_STATUS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(updatedCortprocess);

        restCortprocessMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cortprocessDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isOk());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
        Cortprocess testCortprocess = cortprocessList.get(cortprocessList.size() - 1);
        assertThat(testCortprocess.getProcessCd()).isEqualTo(UPDATED_PROCESS_CD);
        assertThat(testCortprocess.getRefProcessCd()).isEqualTo(UPDATED_REF_PROCESS_CD);
        assertThat(testCortprocess.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testCortprocess.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCortprocess.getLastTime()).isEqualTo(UPDATED_LAST_TIME);
        assertThat(testCortprocess.getLastId()).isEqualTo(UPDATED_LAST_ID);
        assertThat(testCortprocess.getAllowFlag()).isEqualTo(UPDATED_ALLOW_FLAG);
        assertThat(testCortprocess.getCounter()).isEqualTo(UPDATED_COUNTER);
        assertThat(testCortprocess.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCortprocess.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testCortprocess.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testCortprocess.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testCortprocess.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cortprocessDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cortprocessDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCortprocessWithPatch() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();

        // Update the cortprocess using partial update
        Cortprocess partialUpdatedCortprocess = new Cortprocess();
        partialUpdatedCortprocess.setId(cortprocess.getId());

        partialUpdatedCortprocess
            .refProcessCd(UPDATED_REF_PROCESS_CD)
            .name(UPDATED_NAME)
            .lastTime(UPDATED_LAST_TIME)
            .counter(UPDATED_COUNTER)
            .status(UPDATED_STATUS)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restCortprocessMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCortprocess.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCortprocess))
            )
            .andExpect(status().isOk());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
        Cortprocess testCortprocess = cortprocessList.get(cortprocessList.size() - 1);
        assertThat(testCortprocess.getProcessCd()).isEqualTo(DEFAULT_PROCESS_CD);
        assertThat(testCortprocess.getRefProcessCd()).isEqualTo(UPDATED_REF_PROCESS_CD);
        assertThat(testCortprocess.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testCortprocess.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCortprocess.getLastTime()).isEqualTo(UPDATED_LAST_TIME);
        assertThat(testCortprocess.getLastId()).isEqualTo(DEFAULT_LAST_ID);
        assertThat(testCortprocess.getAllowFlag()).isEqualTo(DEFAULT_ALLOW_FLAG);
        assertThat(testCortprocess.getCounter()).isEqualTo(UPDATED_COUNTER);
        assertThat(testCortprocess.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCortprocess.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testCortprocess.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testCortprocess.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testCortprocess.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateCortprocessWithPatch() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();

        // Update the cortprocess using partial update
        Cortprocess partialUpdatedCortprocess = new Cortprocess();
        partialUpdatedCortprocess.setId(cortprocess.getId());

        partialUpdatedCortprocess
            .processCd(UPDATED_PROCESS_CD)
            .refProcessCd(UPDATED_REF_PROCESS_CD)
            .type(UPDATED_TYPE)
            .name(UPDATED_NAME)
            .lastTime(UPDATED_LAST_TIME)
            .lastId(UPDATED_LAST_ID)
            .allowFlag(UPDATED_ALLOW_FLAG)
            .counter(UPDATED_COUNTER)
            .status(UPDATED_STATUS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restCortprocessMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCortprocess.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCortprocess))
            )
            .andExpect(status().isOk());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
        Cortprocess testCortprocess = cortprocessList.get(cortprocessList.size() - 1);
        assertThat(testCortprocess.getProcessCd()).isEqualTo(UPDATED_PROCESS_CD);
        assertThat(testCortprocess.getRefProcessCd()).isEqualTo(UPDATED_REF_PROCESS_CD);
        assertThat(testCortprocess.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testCortprocess.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCortprocess.getLastTime()).isEqualTo(UPDATED_LAST_TIME);
        assertThat(testCortprocess.getLastId()).isEqualTo(UPDATED_LAST_ID);
        assertThat(testCortprocess.getAllowFlag()).isEqualTo(UPDATED_ALLOW_FLAG);
        assertThat(testCortprocess.getCounter()).isEqualTo(UPDATED_COUNTER);
        assertThat(testCortprocess.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCortprocess.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testCortprocess.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testCortprocess.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testCortprocess.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, cortprocessDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCortprocess() throws Exception {
        int databaseSizeBeforeUpdate = cortprocessRepository.findAll().size();
        cortprocess.setId(count.incrementAndGet());

        // Create the Cortprocess
        CortprocessDTO cortprocessDTO = cortprocessMapper.toDto(cortprocess);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCortprocessMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(cortprocessDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cortprocess in the database
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCortprocess() throws Exception {
        // Initialize the database
        cortprocessRepository.saveAndFlush(cortprocess);

        int databaseSizeBeforeDelete = cortprocessRepository.findAll().size();

        // Delete the cortprocess
        restCortprocessMockMvc
            .perform(delete(ENTITY_API_URL_ID, cortprocess.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cortprocess> cortprocessList = cortprocessRepository.findAll();
        assertThat(cortprocessList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
