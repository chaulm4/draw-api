package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.Msttdepartment;
import vn.goline.cp24.api.repository.MsttdepartmentRepository;
import vn.goline.cp24.api.service.dto.MsttdepartmentDTO;
import vn.goline.cp24.api.service.mapper.MsttdepartmentMapper;

/**
 * Integration tests for the {@link MsttdepartmentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MsttdepartmentResourceIT {

    private static final String DEFAULT_DEPARTMENT_CD = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_CD = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BROKER_CD = "AAAAAAAAAA";
    private static final String UPDATED_BROKER_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH_CD = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_CD = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/msttdepartments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MsttdepartmentRepository msttdepartmentRepository;

    @Autowired
    private MsttdepartmentMapper msttdepartmentMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMsttdepartmentMockMvc;

    private Msttdepartment msttdepartment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttdepartment createEntity(EntityManager em) {
        Msttdepartment msttdepartment = new Msttdepartment()
            .departmentCd(DEFAULT_DEPARTMENT_CD)
            .departmentName(DEFAULT_DEPARTMENT_NAME)
            .brokerCd(DEFAULT_BROKER_CD)
            .branchCd(DEFAULT_BRANCH_CD)
            .remarks(DEFAULT_REMARKS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return msttdepartment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Msttdepartment createUpdatedEntity(EntityManager em) {
        Msttdepartment msttdepartment = new Msttdepartment()
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .brokerCd(UPDATED_BROKER_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return msttdepartment;
    }

    @BeforeEach
    public void initTest() {
        msttdepartment = createEntity(em);
    }

    @Test
    @Transactional
    void createMsttdepartment() throws Exception {
        int databaseSizeBeforeCreate = msttdepartmentRepository.findAll().size();
        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);
        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeCreate + 1);
        Msttdepartment testMsttdepartment = msttdepartmentList.get(msttdepartmentList.size() - 1);
        assertThat(testMsttdepartment.getDepartmentCd()).isEqualTo(DEFAULT_DEPARTMENT_CD);
        assertThat(testMsttdepartment.getDepartmentName()).isEqualTo(DEFAULT_DEPARTMENT_NAME);
        assertThat(testMsttdepartment.getBrokerCd()).isEqualTo(DEFAULT_BROKER_CD);
        assertThat(testMsttdepartment.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttdepartment.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttdepartment.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttdepartment.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttdepartment.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttdepartment.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createMsttdepartmentWithExistingId() throws Exception {
        // Create the Msttdepartment with an existing ID
        msttdepartment.setId(1L);
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        int databaseSizeBeforeCreate = msttdepartmentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDepartmentCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setDepartmentCd(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDepartmentNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setDepartmentName(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBrokerCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setBrokerCd(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRemarksIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setRemarks(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setRegDateTime(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setRegUserId(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setUpdDateTime(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttdepartmentRepository.findAll().size();
        // set the field null
        msttdepartment.setUpdUserId(null);

        // Create the Msttdepartment, which fails.
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMsttdepartments() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        // Get all the msttdepartmentList
        restMsttdepartmentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msttdepartment.getId().intValue())))
            .andExpect(jsonPath("$.[*].departmentCd").value(hasItem(DEFAULT_DEPARTMENT_CD)))
            .andExpect(jsonPath("$.[*].departmentName").value(hasItem(DEFAULT_DEPARTMENT_NAME)))
            .andExpect(jsonPath("$.[*].brokerCd").value(hasItem(DEFAULT_BROKER_CD)))
            .andExpect(jsonPath("$.[*].branchCd").value(hasItem(DEFAULT_BRANCH_CD)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getMsttdepartment() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        // Get the msttdepartment
        restMsttdepartmentMockMvc
            .perform(get(ENTITY_API_URL_ID, msttdepartment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(msttdepartment.getId().intValue()))
            .andExpect(jsonPath("$.departmentCd").value(DEFAULT_DEPARTMENT_CD))
            .andExpect(jsonPath("$.departmentName").value(DEFAULT_DEPARTMENT_NAME))
            .andExpect(jsonPath("$.brokerCd").value(DEFAULT_BROKER_CD))
            .andExpect(jsonPath("$.branchCd").value(DEFAULT_BRANCH_CD))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingMsttdepartment() throws Exception {
        // Get the msttdepartment
        restMsttdepartmentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMsttdepartment() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();

        // Update the msttdepartment
        Msttdepartment updatedMsttdepartment = msttdepartmentRepository.findById(msttdepartment.getId()).get();
        // Disconnect from session so that the updates on updatedMsttdepartment are not directly saved in db
        em.detach(updatedMsttdepartment);
        updatedMsttdepartment
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .brokerCd(UPDATED_BROKER_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(updatedMsttdepartment);

        restMsttdepartmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttdepartmentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isOk());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
        Msttdepartment testMsttdepartment = msttdepartmentList.get(msttdepartmentList.size() - 1);
        assertThat(testMsttdepartment.getDepartmentCd()).isEqualTo(UPDATED_DEPARTMENT_CD);
        assertThat(testMsttdepartment.getDepartmentName()).isEqualTo(UPDATED_DEPARTMENT_NAME);
        assertThat(testMsttdepartment.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttdepartment.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttdepartment.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttdepartment.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttdepartment.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttdepartment.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttdepartment.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttdepartmentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMsttdepartmentWithPatch() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();

        // Update the msttdepartment using partial update
        Msttdepartment partialUpdatedMsttdepartment = new Msttdepartment();
        partialUpdatedMsttdepartment.setId(msttdepartment.getId());

        partialUpdatedMsttdepartment
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .brokerCd(UPDATED_BROKER_CD)
            .regUserId(UPDATED_REG_USER_ID);

        restMsttdepartmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttdepartment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttdepartment))
            )
            .andExpect(status().isOk());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
        Msttdepartment testMsttdepartment = msttdepartmentList.get(msttdepartmentList.size() - 1);
        assertThat(testMsttdepartment.getDepartmentCd()).isEqualTo(UPDATED_DEPARTMENT_CD);
        assertThat(testMsttdepartment.getDepartmentName()).isEqualTo(UPDATED_DEPARTMENT_NAME);
        assertThat(testMsttdepartment.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttdepartment.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttdepartment.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttdepartment.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttdepartment.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttdepartment.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttdepartment.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMsttdepartmentWithPatch() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();

        // Update the msttdepartment using partial update
        Msttdepartment partialUpdatedMsttdepartment = new Msttdepartment();
        partialUpdatedMsttdepartment.setId(msttdepartment.getId());

        partialUpdatedMsttdepartment
            .departmentCd(UPDATED_DEPARTMENT_CD)
            .departmentName(UPDATED_DEPARTMENT_NAME)
            .brokerCd(UPDATED_BROKER_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttdepartmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttdepartment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttdepartment))
            )
            .andExpect(status().isOk());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
        Msttdepartment testMsttdepartment = msttdepartmentList.get(msttdepartmentList.size() - 1);
        assertThat(testMsttdepartment.getDepartmentCd()).isEqualTo(UPDATED_DEPARTMENT_CD);
        assertThat(testMsttdepartment.getDepartmentName()).isEqualTo(UPDATED_DEPARTMENT_NAME);
        assertThat(testMsttdepartment.getBrokerCd()).isEqualTo(UPDATED_BROKER_CD);
        assertThat(testMsttdepartment.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttdepartment.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttdepartment.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttdepartment.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttdepartment.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttdepartment.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, msttdepartmentDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMsttdepartment() throws Exception {
        int databaseSizeBeforeUpdate = msttdepartmentRepository.findAll().size();
        msttdepartment.setId(count.incrementAndGet());

        // Create the Msttdepartment
        MsttdepartmentDTO msttdepartmentDTO = msttdepartmentMapper.toDto(msttdepartment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttdepartmentMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttdepartmentDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Msttdepartment in the database
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMsttdepartment() throws Exception {
        // Initialize the database
        msttdepartmentRepository.saveAndFlush(msttdepartment);

        int databaseSizeBeforeDelete = msttdepartmentRepository.findAll().size();

        // Delete the msttdepartment
        restMsttdepartmentMockMvc
            .perform(delete(ENTITY_API_URL_ID, msttdepartment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Msttdepartment> msttdepartmentList = msttdepartmentRepository.findAll();
        assertThat(msttdepartmentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
