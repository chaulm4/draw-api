package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.MsttblockDetails;
import vn.goline.cp24.api.repository.MsttblockDetailsRepository;
import vn.goline.cp24.api.service.dto.MsttblockDetailsDTO;
import vn.goline.cp24.api.service.mapper.MsttblockDetailsMapper;

/**
 * Integration tests for the {@link MsttblockDetailsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MsttblockDetailsResourceIT {

    private static final Long DEFAULT_REF_ID = 8L;
    private static final Long UPDATED_REF_ID = 7L;

    private static final String DEFAULT_BLOCK_CD = "AAAAAAAAAA";
    private static final String UPDATED_BLOCK_CD = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH_CD = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_CD = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 0;

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/msttblock-details";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MsttblockDetailsRepository msttblockDetailsRepository;

    @Autowired
    private MsttblockDetailsMapper msttblockDetailsMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMsttblockDetailsMockMvc;

    private MsttblockDetails msttblockDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MsttblockDetails createEntity(EntityManager em) {
        MsttblockDetails msttblockDetails = new MsttblockDetails()
            .refId(DEFAULT_REF_ID)
            .blockCd(DEFAULT_BLOCK_CD)
            .branchCd(DEFAULT_BRANCH_CD)
            .status(DEFAULT_STATUS)
            .remarks(DEFAULT_REMARKS)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return msttblockDetails;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MsttblockDetails createUpdatedEntity(EntityManager em) {
        MsttblockDetails msttblockDetails = new MsttblockDetails()
            .refId(UPDATED_REF_ID)
            .blockCd(UPDATED_BLOCK_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .status(UPDATED_STATUS)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return msttblockDetails;
    }

    @BeforeEach
    public void initTest() {
        msttblockDetails = createEntity(em);
    }

    @Test
    @Transactional
    void createMsttblockDetails() throws Exception {
        int databaseSizeBeforeCreate = msttblockDetailsRepository.findAll().size();
        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);
        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        MsttblockDetails testMsttblockDetails = msttblockDetailsList.get(msttblockDetailsList.size() - 1);
        assertThat(testMsttblockDetails.getRefId()).isEqualTo(DEFAULT_REF_ID);
        assertThat(testMsttblockDetails.getBlockCd()).isEqualTo(DEFAULT_BLOCK_CD);
        assertThat(testMsttblockDetails.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttblockDetails.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testMsttblockDetails.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testMsttblockDetails.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testMsttblockDetails.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttblockDetails.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testMsttblockDetails.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createMsttblockDetailsWithExistingId() throws Exception {
        // Create the MsttblockDetails with an existing ID
        msttblockDetails.setId(1L);
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        int databaseSizeBeforeCreate = msttblockDetailsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkBlockCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setBlockCd(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBranchCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setBranchCd(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setStatus(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRemarksIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setRemarks(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setRegDateTime(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkRegUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setRegUserId(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdDateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setUpdDateTime(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = msttblockDetailsRepository.findAll().size();
        // set the field null
        msttblockDetails.setUpdUserId(null);

        // Create the MsttblockDetails, which fails.
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMsttblockDetails() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        // Get all the msttblockDetailsList
        restMsttblockDetailsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msttblockDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].refId").value(hasItem(DEFAULT_REF_ID.intValue())))
            .andExpect(jsonPath("$.[*].blockCd").value(hasItem(DEFAULT_BLOCK_CD)))
            .andExpect(jsonPath("$.[*].branchCd").value(hasItem(DEFAULT_BRANCH_CD)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getMsttblockDetails() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        // Get the msttblockDetails
        restMsttblockDetailsMockMvc
            .perform(get(ENTITY_API_URL_ID, msttblockDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(msttblockDetails.getId().intValue()))
            .andExpect(jsonPath("$.refId").value(DEFAULT_REF_ID.intValue()))
            .andExpect(jsonPath("$.blockCd").value(DEFAULT_BLOCK_CD))
            .andExpect(jsonPath("$.branchCd").value(DEFAULT_BRANCH_CD))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingMsttblockDetails() throws Exception {
        // Get the msttblockDetails
        restMsttblockDetailsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMsttblockDetails() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();

        // Update the msttblockDetails
        MsttblockDetails updatedMsttblockDetails = msttblockDetailsRepository.findById(msttblockDetails.getId()).get();
        // Disconnect from session so that the updates on updatedMsttblockDetails are not directly saved in db
        em.detach(updatedMsttblockDetails);
        updatedMsttblockDetails
            .refId(UPDATED_REF_ID)
            .blockCd(UPDATED_BLOCK_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .status(UPDATED_STATUS)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(updatedMsttblockDetails);

        restMsttblockDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttblockDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isOk());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
        MsttblockDetails testMsttblockDetails = msttblockDetailsList.get(msttblockDetailsList.size() - 1);
        assertThat(testMsttblockDetails.getRefId()).isEqualTo(UPDATED_REF_ID);
        assertThat(testMsttblockDetails.getBlockCd()).isEqualTo(UPDATED_BLOCK_CD);
        assertThat(testMsttblockDetails.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttblockDetails.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMsttblockDetails.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttblockDetails.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblockDetails.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttblockDetails.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttblockDetails.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, msttblockDetailsDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMsttblockDetailsWithPatch() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();

        // Update the msttblockDetails using partial update
        MsttblockDetails partialUpdatedMsttblockDetails = new MsttblockDetails();
        partialUpdatedMsttblockDetails.setId(msttblockDetails.getId());

        partialUpdatedMsttblockDetails
            .blockCd(UPDATED_BLOCK_CD)
            .status(UPDATED_STATUS)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .updDateTime(UPDATED_UPD_DATE_TIME);

        restMsttblockDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttblockDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttblockDetails))
            )
            .andExpect(status().isOk());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
        MsttblockDetails testMsttblockDetails = msttblockDetailsList.get(msttblockDetailsList.size() - 1);
        assertThat(testMsttblockDetails.getRefId()).isEqualTo(DEFAULT_REF_ID);
        assertThat(testMsttblockDetails.getBlockCd()).isEqualTo(UPDATED_BLOCK_CD);
        assertThat(testMsttblockDetails.getBranchCd()).isEqualTo(DEFAULT_BRANCH_CD);
        assertThat(testMsttblockDetails.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMsttblockDetails.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttblockDetails.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblockDetails.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testMsttblockDetails.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttblockDetails.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMsttblockDetailsWithPatch() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();

        // Update the msttblockDetails using partial update
        MsttblockDetails partialUpdatedMsttblockDetails = new MsttblockDetails();
        partialUpdatedMsttblockDetails.setId(msttblockDetails.getId());

        partialUpdatedMsttblockDetails
            .refId(UPDATED_REF_ID)
            .blockCd(UPDATED_BLOCK_CD)
            .branchCd(UPDATED_BRANCH_CD)
            .status(UPDATED_STATUS)
            .remarks(UPDATED_REMARKS)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restMsttblockDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMsttblockDetails.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMsttblockDetails))
            )
            .andExpect(status().isOk());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
        MsttblockDetails testMsttblockDetails = msttblockDetailsList.get(msttblockDetailsList.size() - 1);
        assertThat(testMsttblockDetails.getRefId()).isEqualTo(UPDATED_REF_ID);
        assertThat(testMsttblockDetails.getBlockCd()).isEqualTo(UPDATED_BLOCK_CD);
        assertThat(testMsttblockDetails.getBranchCd()).isEqualTo(UPDATED_BRANCH_CD);
        assertThat(testMsttblockDetails.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testMsttblockDetails.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testMsttblockDetails.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testMsttblockDetails.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testMsttblockDetails.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testMsttblockDetails.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, msttblockDetailsDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMsttblockDetails() throws Exception {
        int databaseSizeBeforeUpdate = msttblockDetailsRepository.findAll().size();
        msttblockDetails.setId(count.incrementAndGet());

        // Create the MsttblockDetails
        MsttblockDetailsDTO msttblockDetailsDTO = msttblockDetailsMapper.toDto(msttblockDetails);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMsttblockDetailsMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(msttblockDetailsDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MsttblockDetails in the database
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMsttblockDetails() throws Exception {
        // Initialize the database
        msttblockDetailsRepository.saveAndFlush(msttblockDetails);

        int databaseSizeBeforeDelete = msttblockDetailsRepository.findAll().size();

        // Delete the msttblockDetails
        restMsttblockDetailsMockMvc
            .perform(delete(ENTITY_API_URL_ID, msttblockDetails.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MsttblockDetails> msttblockDetailsList = msttblockDetailsRepository.findAll();
        assertThat(msttblockDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
