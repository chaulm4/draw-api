package vn.goline.cp24.api.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.IntegrationTest;
import vn.goline.cp24.api.domain.OrdttradingSession;
import vn.goline.cp24.api.repository.OrdttradingSessionRepository;
import vn.goline.cp24.api.service.dto.OrdttradingSessionDTO;
import vn.goline.cp24.api.service.mapper.OrdttradingSessionMapper;

/**
 * Integration tests for the {@link OrdttradingSessionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrdttradingSessionResourceIT {

    private static final String DEFAULT_TRADING_SESSION_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRADING_SESSION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TRADING_SESSION_SUB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRADING_SESSION_SUB_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_TRAD_DATE = 1;
    private static final Integer UPDATED_TRAD_DATE = 2;

    private static final String DEFAULT_SESSION_CD = "AAAAAAAAAA";
    private static final String UPDATED_SESSION_CD = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Integer DEFAULT_TRADING_FLAG = 1;
    private static final Integer UPDATED_TRADING_FLAG = 2;

    private static final Instant DEFAULT_REG_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REG_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_REG_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_REG_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPD_DATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPD_DATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPD_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_UPD_USER_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/ordttrading-sessions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrdttradingSessionRepository ordttradingSessionRepository;

    @Autowired
    private OrdttradingSessionMapper ordttradingSessionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrdttradingSessionMockMvc;

    private OrdttradingSession ordttradingSession;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrdttradingSession createEntity(EntityManager em) {
        OrdttradingSession ordttradingSession = new OrdttradingSession()
            .tradingSessionId(DEFAULT_TRADING_SESSION_ID)
            .tradingSessionSubId(DEFAULT_TRADING_SESSION_SUB_ID)
            .tradDate(DEFAULT_TRAD_DATE)
            .sessionCd(DEFAULT_SESSION_CD)
            .status(DEFAULT_STATUS)
            .tradingFlag(DEFAULT_TRADING_FLAG)
            .regDateTime(DEFAULT_REG_DATE_TIME)
            .regUserId(DEFAULT_REG_USER_ID)
            .updDateTime(DEFAULT_UPD_DATE_TIME)
            .updUserId(DEFAULT_UPD_USER_ID);
        return ordttradingSession;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrdttradingSession createUpdatedEntity(EntityManager em) {
        OrdttradingSession ordttradingSession = new OrdttradingSession()
            .tradingSessionId(UPDATED_TRADING_SESSION_ID)
            .tradingSessionSubId(UPDATED_TRADING_SESSION_SUB_ID)
            .tradDate(UPDATED_TRAD_DATE)
            .sessionCd(UPDATED_SESSION_CD)
            .status(UPDATED_STATUS)
            .tradingFlag(UPDATED_TRADING_FLAG)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        return ordttradingSession;
    }

    @BeforeEach
    public void initTest() {
        ordttradingSession = createEntity(em);
    }

    @Test
    @Transactional
    void createOrdttradingSession() throws Exception {
        int databaseSizeBeforeCreate = ordttradingSessionRepository.findAll().size();
        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);
        restOrdttradingSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeCreate + 1);
        OrdttradingSession testOrdttradingSession = ordttradingSessionList.get(ordttradingSessionList.size() - 1);
        assertThat(testOrdttradingSession.getTradingSessionId()).isEqualTo(DEFAULT_TRADING_SESSION_ID);
        assertThat(testOrdttradingSession.getTradingSessionSubId()).isEqualTo(DEFAULT_TRADING_SESSION_SUB_ID);
        assertThat(testOrdttradingSession.getTradDate()).isEqualTo(DEFAULT_TRAD_DATE);
        assertThat(testOrdttradingSession.getSessionCd()).isEqualTo(DEFAULT_SESSION_CD);
        assertThat(testOrdttradingSession.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrdttradingSession.getTradingFlag()).isEqualTo(DEFAULT_TRADING_FLAG);
        assertThat(testOrdttradingSession.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testOrdttradingSession.getRegUserId()).isEqualTo(DEFAULT_REG_USER_ID);
        assertThat(testOrdttradingSession.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testOrdttradingSession.getUpdUserId()).isEqualTo(DEFAULT_UPD_USER_ID);
    }

    @Test
    @Transactional
    void createOrdttradingSessionWithExistingId() throws Exception {
        // Create the OrdttradingSession with an existing ID
        ordttradingSession.setId(1L);
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        int databaseSizeBeforeCreate = ordttradingSessionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrdttradingSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTradingSessionIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = ordttradingSessionRepository.findAll().size();
        // set the field null
        ordttradingSession.setTradingSessionId(null);

        // Create the OrdttradingSession, which fails.
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        restOrdttradingSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSessionCdIsRequired() throws Exception {
        int databaseSizeBeforeTest = ordttradingSessionRepository.findAll().size();
        // set the field null
        ordttradingSession.setSessionCd(null);

        // Create the OrdttradingSession, which fails.
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        restOrdttradingSessionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllOrdttradingSessions() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        // Get all the ordttradingSessionList
        restOrdttradingSessionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ordttradingSession.getId().intValue())))
            .andExpect(jsonPath("$.[*].tradingSessionId").value(hasItem(DEFAULT_TRADING_SESSION_ID)))
            .andExpect(jsonPath("$.[*].tradingSessionSubId").value(hasItem(DEFAULT_TRADING_SESSION_SUB_ID)))
            .andExpect(jsonPath("$.[*].tradDate").value(hasItem(DEFAULT_TRAD_DATE)))
            .andExpect(jsonPath("$.[*].sessionCd").value(hasItem(DEFAULT_SESSION_CD)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].tradingFlag").value(hasItem(DEFAULT_TRADING_FLAG)))
            .andExpect(jsonPath("$.[*].regDateTime").value(hasItem(DEFAULT_REG_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].regUserId").value(hasItem(DEFAULT_REG_USER_ID)))
            .andExpect(jsonPath("$.[*].updDateTime").value(hasItem(DEFAULT_UPD_DATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].updUserId").value(hasItem(DEFAULT_UPD_USER_ID)));
    }

    @Test
    @Transactional
    void getOrdttradingSession() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        // Get the ordttradingSession
        restOrdttradingSessionMockMvc
            .perform(get(ENTITY_API_URL_ID, ordttradingSession.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ordttradingSession.getId().intValue()))
            .andExpect(jsonPath("$.tradingSessionId").value(DEFAULT_TRADING_SESSION_ID))
            .andExpect(jsonPath("$.tradingSessionSubId").value(DEFAULT_TRADING_SESSION_SUB_ID))
            .andExpect(jsonPath("$.tradDate").value(DEFAULT_TRAD_DATE))
            .andExpect(jsonPath("$.sessionCd").value(DEFAULT_SESSION_CD))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.tradingFlag").value(DEFAULT_TRADING_FLAG))
            .andExpect(jsonPath("$.regDateTime").value(DEFAULT_REG_DATE_TIME.toString()))
            .andExpect(jsonPath("$.regUserId").value(DEFAULT_REG_USER_ID))
            .andExpect(jsonPath("$.updDateTime").value(DEFAULT_UPD_DATE_TIME.toString()))
            .andExpect(jsonPath("$.updUserId").value(DEFAULT_UPD_USER_ID));
    }

    @Test
    @Transactional
    void getNonExistingOrdttradingSession() throws Exception {
        // Get the ordttradingSession
        restOrdttradingSessionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOrdttradingSession() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();

        // Update the ordttradingSession
        OrdttradingSession updatedOrdttradingSession = ordttradingSessionRepository.findById(ordttradingSession.getId()).get();
        // Disconnect from session so that the updates on updatedOrdttradingSession are not directly saved in db
        em.detach(updatedOrdttradingSession);
        updatedOrdttradingSession
            .tradingSessionId(UPDATED_TRADING_SESSION_ID)
            .tradingSessionSubId(UPDATED_TRADING_SESSION_SUB_ID)
            .tradDate(UPDATED_TRAD_DATE)
            .sessionCd(UPDATED_SESSION_CD)
            .status(UPDATED_STATUS)
            .tradingFlag(UPDATED_TRADING_FLAG)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(updatedOrdttradingSession);

        restOrdttradingSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ordttradingSessionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isOk());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
        OrdttradingSession testOrdttradingSession = ordttradingSessionList.get(ordttradingSessionList.size() - 1);
        assertThat(testOrdttradingSession.getTradingSessionId()).isEqualTo(UPDATED_TRADING_SESSION_ID);
        assertThat(testOrdttradingSession.getTradingSessionSubId()).isEqualTo(UPDATED_TRADING_SESSION_SUB_ID);
        assertThat(testOrdttradingSession.getTradDate()).isEqualTo(UPDATED_TRAD_DATE);
        assertThat(testOrdttradingSession.getSessionCd()).isEqualTo(UPDATED_SESSION_CD);
        assertThat(testOrdttradingSession.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrdttradingSession.getTradingFlag()).isEqualTo(UPDATED_TRADING_FLAG);
        assertThat(testOrdttradingSession.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testOrdttradingSession.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testOrdttradingSession.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testOrdttradingSession.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ordttradingSessionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrdttradingSessionWithPatch() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();

        // Update the ordttradingSession using partial update
        OrdttradingSession partialUpdatedOrdttradingSession = new OrdttradingSession();
        partialUpdatedOrdttradingSession.setId(ordttradingSession.getId());

        partialUpdatedOrdttradingSession
            .status(UPDATED_STATUS)
            .tradingFlag(UPDATED_TRADING_FLAG)
            .regUserId(UPDATED_REG_USER_ID)
            .updUserId(UPDATED_UPD_USER_ID);

        restOrdttradingSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrdttradingSession.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrdttradingSession))
            )
            .andExpect(status().isOk());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
        OrdttradingSession testOrdttradingSession = ordttradingSessionList.get(ordttradingSessionList.size() - 1);
        assertThat(testOrdttradingSession.getTradingSessionId()).isEqualTo(DEFAULT_TRADING_SESSION_ID);
        assertThat(testOrdttradingSession.getTradingSessionSubId()).isEqualTo(DEFAULT_TRADING_SESSION_SUB_ID);
        assertThat(testOrdttradingSession.getTradDate()).isEqualTo(DEFAULT_TRAD_DATE);
        assertThat(testOrdttradingSession.getSessionCd()).isEqualTo(DEFAULT_SESSION_CD);
        assertThat(testOrdttradingSession.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrdttradingSession.getTradingFlag()).isEqualTo(UPDATED_TRADING_FLAG);
        assertThat(testOrdttradingSession.getRegDateTime()).isEqualTo(DEFAULT_REG_DATE_TIME);
        assertThat(testOrdttradingSession.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testOrdttradingSession.getUpdDateTime()).isEqualTo(DEFAULT_UPD_DATE_TIME);
        assertThat(testOrdttradingSession.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateOrdttradingSessionWithPatch() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();

        // Update the ordttradingSession using partial update
        OrdttradingSession partialUpdatedOrdttradingSession = new OrdttradingSession();
        partialUpdatedOrdttradingSession.setId(ordttradingSession.getId());

        partialUpdatedOrdttradingSession
            .tradingSessionId(UPDATED_TRADING_SESSION_ID)
            .tradingSessionSubId(UPDATED_TRADING_SESSION_SUB_ID)
            .tradDate(UPDATED_TRAD_DATE)
            .sessionCd(UPDATED_SESSION_CD)
            .status(UPDATED_STATUS)
            .tradingFlag(UPDATED_TRADING_FLAG)
            .regDateTime(UPDATED_REG_DATE_TIME)
            .regUserId(UPDATED_REG_USER_ID)
            .updDateTime(UPDATED_UPD_DATE_TIME)
            .updUserId(UPDATED_UPD_USER_ID);

        restOrdttradingSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrdttradingSession.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrdttradingSession))
            )
            .andExpect(status().isOk());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
        OrdttradingSession testOrdttradingSession = ordttradingSessionList.get(ordttradingSessionList.size() - 1);
        assertThat(testOrdttradingSession.getTradingSessionId()).isEqualTo(UPDATED_TRADING_SESSION_ID);
        assertThat(testOrdttradingSession.getTradingSessionSubId()).isEqualTo(UPDATED_TRADING_SESSION_SUB_ID);
        assertThat(testOrdttradingSession.getTradDate()).isEqualTo(UPDATED_TRAD_DATE);
        assertThat(testOrdttradingSession.getSessionCd()).isEqualTo(UPDATED_SESSION_CD);
        assertThat(testOrdttradingSession.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrdttradingSession.getTradingFlag()).isEqualTo(UPDATED_TRADING_FLAG);
        assertThat(testOrdttradingSession.getRegDateTime()).isEqualTo(UPDATED_REG_DATE_TIME);
        assertThat(testOrdttradingSession.getRegUserId()).isEqualTo(UPDATED_REG_USER_ID);
        assertThat(testOrdttradingSession.getUpdDateTime()).isEqualTo(UPDATED_UPD_DATE_TIME);
        assertThat(testOrdttradingSession.getUpdUserId()).isEqualTo(UPDATED_UPD_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ordttradingSessionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrdttradingSession() throws Exception {
        int databaseSizeBeforeUpdate = ordttradingSessionRepository.findAll().size();
        ordttradingSession.setId(count.incrementAndGet());

        // Create the OrdttradingSession
        OrdttradingSessionDTO ordttradingSessionDTO = ordttradingSessionMapper.toDto(ordttradingSession);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrdttradingSessionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ordttradingSessionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrdttradingSession in the database
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrdttradingSession() throws Exception {
        // Initialize the database
        ordttradingSessionRepository.saveAndFlush(ordttradingSession);

        int databaseSizeBeforeDelete = ordttradingSessionRepository.findAll().size();

        // Delete the ordttradingSession
        restOrdttradingSessionMockMvc
            .perform(delete(ENTITY_API_URL_ID, ordttradingSession.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrdttradingSession> ordttradingSessionList = ordttradingSessionRepository.findAll();
        assertThat(ordttradingSessionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
