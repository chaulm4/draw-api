package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class CortprocessDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CortprocessDTO.class);
        CortprocessDTO cortprocessDTO1 = new CortprocessDTO();
        cortprocessDTO1.setId(1L);
        CortprocessDTO cortprocessDTO2 = new CortprocessDTO();
        assertThat(cortprocessDTO1).isNotEqualTo(cortprocessDTO2);
        cortprocessDTO2.setId(cortprocessDTO1.getId());
        assertThat(cortprocessDTO1).isEqualTo(cortprocessDTO2);
        cortprocessDTO2.setId(2L);
        assertThat(cortprocessDTO1).isNotEqualTo(cortprocessDTO2);
        cortprocessDTO1.setId(null);
        assertThat(cortprocessDTO1).isNotEqualTo(cortprocessDTO2);
    }
}
