package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class OrdttradingSessionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrdttradingSessionDTO.class);
        OrdttradingSessionDTO ordttradingSessionDTO1 = new OrdttradingSessionDTO();
        ordttradingSessionDTO1.setId(1L);
        OrdttradingSessionDTO ordttradingSessionDTO2 = new OrdttradingSessionDTO();
        assertThat(ordttradingSessionDTO1).isNotEqualTo(ordttradingSessionDTO2);
        ordttradingSessionDTO2.setId(ordttradingSessionDTO1.getId());
        assertThat(ordttradingSessionDTO1).isEqualTo(ordttradingSessionDTO2);
        ordttradingSessionDTO2.setId(2L);
        assertThat(ordttradingSessionDTO1).isNotEqualTo(ordttradingSessionDTO2);
        ordttradingSessionDTO1.setId(null);
        assertThat(ordttradingSessionDTO1).isNotEqualTo(ordttradingSessionDTO2);
    }
}
