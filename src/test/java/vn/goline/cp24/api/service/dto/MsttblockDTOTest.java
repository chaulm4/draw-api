package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttblockDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttblockDTO.class);
        MsttblockDTO msttblockDTO1 = new MsttblockDTO();
        msttblockDTO1.setId(1L);
        MsttblockDTO msttblockDTO2 = new MsttblockDTO();
        assertThat(msttblockDTO1).isNotEqualTo(msttblockDTO2);
        msttblockDTO2.setId(msttblockDTO1.getId());
        assertThat(msttblockDTO1).isEqualTo(msttblockDTO2);
        msttblockDTO2.setId(2L);
        assertThat(msttblockDTO1).isNotEqualTo(msttblockDTO2);
        msttblockDTO1.setId(null);
        assertThat(msttblockDTO1).isNotEqualTo(msttblockDTO2);
    }
}
