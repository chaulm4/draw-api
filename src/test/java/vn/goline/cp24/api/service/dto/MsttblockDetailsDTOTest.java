package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttblockDetailsDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttblockDetailsDTO.class);
        MsttblockDetailsDTO msttblockDetailsDTO1 = new MsttblockDetailsDTO();
        msttblockDetailsDTO1.setId(1L);
        MsttblockDetailsDTO msttblockDetailsDTO2 = new MsttblockDetailsDTO();
        assertThat(msttblockDetailsDTO1).isNotEqualTo(msttblockDetailsDTO2);
        msttblockDetailsDTO2.setId(msttblockDetailsDTO1.getId());
        assertThat(msttblockDetailsDTO1).isEqualTo(msttblockDetailsDTO2);
        msttblockDetailsDTO2.setId(2L);
        assertThat(msttblockDetailsDTO1).isNotEqualTo(msttblockDetailsDTO2);
        msttblockDetailsDTO1.setId(null);
        assertThat(msttblockDetailsDTO1).isNotEqualTo(msttblockDetailsDTO2);
    }
}
