package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttbrokerDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttbrokerDTO.class);
        MsttbrokerDTO msttbrokerDTO1 = new MsttbrokerDTO();
        msttbrokerDTO1.setId(1L);
        MsttbrokerDTO msttbrokerDTO2 = new MsttbrokerDTO();
        assertThat(msttbrokerDTO1).isNotEqualTo(msttbrokerDTO2);
        msttbrokerDTO2.setId(msttbrokerDTO1.getId());
        assertThat(msttbrokerDTO1).isEqualTo(msttbrokerDTO2);
        msttbrokerDTO2.setId(2L);
        assertThat(msttbrokerDTO1).isNotEqualTo(msttbrokerDTO2);
        msttbrokerDTO1.setId(null);
        assertThat(msttbrokerDTO1).isNotEqualTo(msttbrokerDTO2);
    }
}
