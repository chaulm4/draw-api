package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttbranchDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttbranchDTO.class);
        MsttbranchDTO msttbranchDTO1 = new MsttbranchDTO();
        msttbranchDTO1.setId(1L);
        MsttbranchDTO msttbranchDTO2 = new MsttbranchDTO();
        assertThat(msttbranchDTO1).isNotEqualTo(msttbranchDTO2);
        msttbranchDTO2.setId(msttbranchDTO1.getId());
        assertThat(msttbranchDTO1).isEqualTo(msttbranchDTO2);
        msttbranchDTO2.setId(2L);
        assertThat(msttbranchDTO1).isNotEqualTo(msttbranchDTO2);
        msttbranchDTO1.setId(null);
        assertThat(msttbranchDTO1).isNotEqualTo(msttbranchDTO2);
    }
}
