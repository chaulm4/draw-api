package vn.goline.cp24.api.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttdepartmentDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttdepartmentDTO.class);
        MsttdepartmentDTO msttdepartmentDTO1 = new MsttdepartmentDTO();
        msttdepartmentDTO1.setId(1L);
        MsttdepartmentDTO msttdepartmentDTO2 = new MsttdepartmentDTO();
        assertThat(msttdepartmentDTO1).isNotEqualTo(msttdepartmentDTO2);
        msttdepartmentDTO2.setId(msttdepartmentDTO1.getId());
        assertThat(msttdepartmentDTO1).isEqualTo(msttdepartmentDTO2);
        msttdepartmentDTO2.setId(2L);
        assertThat(msttdepartmentDTO1).isNotEqualTo(msttdepartmentDTO2);
        msttdepartmentDTO1.setId(null);
        assertThat(msttdepartmentDTO1).isNotEqualTo(msttdepartmentDTO2);
    }
}
