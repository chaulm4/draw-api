package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttdepartmentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Msttdepartment.class);
        Msttdepartment msttdepartment1 = new Msttdepartment();
        msttdepartment1.setId(1L);
        Msttdepartment msttdepartment2 = new Msttdepartment();
        msttdepartment2.setId(msttdepartment1.getId());
        assertThat(msttdepartment1).isEqualTo(msttdepartment2);
        msttdepartment2.setId(2L);
        assertThat(msttdepartment1).isNotEqualTo(msttdepartment2);
        msttdepartment1.setId(null);
        assertThat(msttdepartment1).isNotEqualTo(msttdepartment2);
    }
}
