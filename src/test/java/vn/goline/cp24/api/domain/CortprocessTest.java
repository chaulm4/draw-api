package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class CortprocessTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cortprocess.class);
        Cortprocess cortprocess1 = new Cortprocess();
        cortprocess1.setId(1L);
        Cortprocess cortprocess2 = new Cortprocess();
        cortprocess2.setId(cortprocess1.getId());
        assertThat(cortprocess1).isEqualTo(cortprocess2);
        cortprocess2.setId(2L);
        assertThat(cortprocess1).isNotEqualTo(cortprocess2);
        cortprocess1.setId(null);
        assertThat(cortprocess1).isNotEqualTo(cortprocess2);
    }
}
