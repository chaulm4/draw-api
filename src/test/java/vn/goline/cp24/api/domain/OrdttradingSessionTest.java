package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class OrdttradingSessionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrdttradingSession.class);
        OrdttradingSession ordttradingSession1 = new OrdttradingSession();
        ordttradingSession1.setId(1L);
        OrdttradingSession ordttradingSession2 = new OrdttradingSession();
        ordttradingSession2.setId(ordttradingSession1.getId());
        assertThat(ordttradingSession1).isEqualTo(ordttradingSession2);
        ordttradingSession2.setId(2L);
        assertThat(ordttradingSession1).isNotEqualTo(ordttradingSession2);
        ordttradingSession1.setId(null);
        assertThat(ordttradingSession1).isNotEqualTo(ordttradingSession2);
    }
}
