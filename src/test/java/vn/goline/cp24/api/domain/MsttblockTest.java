package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttblockTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Msttblock.class);
        Msttblock msttblock1 = new Msttblock();
        msttblock1.setId(1L);
        Msttblock msttblock2 = new Msttblock();
        msttblock2.setId(msttblock1.getId());
        assertThat(msttblock1).isEqualTo(msttblock2);
        msttblock2.setId(2L);
        assertThat(msttblock1).isNotEqualTo(msttblock2);
        msttblock1.setId(null);
        assertThat(msttblock1).isNotEqualTo(msttblock2);
    }
}
