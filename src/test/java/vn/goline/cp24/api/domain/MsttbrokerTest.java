package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttbrokerTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Msttbroker.class);
        Msttbroker msttbroker1 = new Msttbroker();
        msttbroker1.setId(1L);
        Msttbroker msttbroker2 = new Msttbroker();
        msttbroker2.setId(msttbroker1.getId());
        assertThat(msttbroker1).isEqualTo(msttbroker2);
        msttbroker2.setId(2L);
        assertThat(msttbroker1).isNotEqualTo(msttbroker2);
        msttbroker1.setId(null);
        assertThat(msttbroker1).isNotEqualTo(msttbroker2);
    }
}
