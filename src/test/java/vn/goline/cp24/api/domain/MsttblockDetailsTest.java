package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttblockDetailsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsttblockDetails.class);
        MsttblockDetails msttblockDetails1 = new MsttblockDetails();
        msttblockDetails1.setId(1L);
        MsttblockDetails msttblockDetails2 = new MsttblockDetails();
        msttblockDetails2.setId(msttblockDetails1.getId());
        assertThat(msttblockDetails1).isEqualTo(msttblockDetails2);
        msttblockDetails2.setId(2L);
        assertThat(msttblockDetails1).isNotEqualTo(msttblockDetails2);
        msttblockDetails1.setId(null);
        assertThat(msttblockDetails1).isNotEqualTo(msttblockDetails2);
    }
}
