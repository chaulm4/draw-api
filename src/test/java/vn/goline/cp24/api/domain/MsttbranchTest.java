package vn.goline.cp24.api.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import vn.goline.cp24.api.web.rest.TestUtil;

class MsttbranchTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Msttbranch.class);
        Msttbranch msttbranch1 = new Msttbranch();
        msttbranch1.setId(1L);
        Msttbranch msttbranch2 = new Msttbranch();
        msttbranch2.setId(msttbranch1.getId());
        assertThat(msttbranch1).isEqualTo(msttbranch2);
        msttbranch2.setId(2L);
        assertThat(msttbranch1).isNotEqualTo(msttbranch2);
        msttbranch1.setId(null);
        assertThat(msttbranch1).isNotEqualTo(msttbranch2);
    }
}
