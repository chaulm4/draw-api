package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.Msttbroker} entity.
 */
@ApiModel(description = "The Msttbroker entity.\n@author A true hipster")
public class MsttbrokerDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String brokerCd;

    @NotNull
    @Size(max = 100)
    private String brokerName;

    @Size(max = 100)
    private String position;

    @Size(max = 100)
    private String managementId;

    @Size(max = 30)
    private String departmentCd;

    @Size(max = 30)
    private String branchCd;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "remarks", required = true)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @ApiModelProperty(value = "regDateTime", required = true)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "regUserId", required = true)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @ApiModelProperty(value = "updDateTime", required = true)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "updUserId", required = true)
    private String updUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrokerCd() {
        return brokerCd;
    }

    public void setBrokerCd(String brokerCd) {
        this.brokerCd = brokerCd;
    }

    public String getBrokerName() {
        return brokerName;
    }

    public void setBrokerName(String brokerName) {
        this.brokerName = brokerName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getManagementId() {
        return managementId;
    }

    public void setManagementId(String managementId) {
        this.managementId = managementId;
    }

    public String getDepartmentCd() {
        return departmentCd;
    }

    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttbrokerDTO)) {
            return false;
        }

        MsttbrokerDTO msttbrokerDTO = (MsttbrokerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, msttbrokerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttbrokerDTO{" +
            "id=" + getId() +
            ", brokerCd='" + getBrokerCd() + "'" +
            ", brokerName='" + getBrokerName() + "'" +
            ", position='" + getPosition() + "'" +
            ", managementId='" + getManagementId() + "'" +
            ", departmentCd='" + getDepartmentCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
