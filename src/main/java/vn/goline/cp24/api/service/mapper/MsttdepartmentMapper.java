package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.MsttdepartmentDTO;

/**
 * Mapper for the entity {@link Msttdepartment} and its DTO {@link MsttdepartmentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MsttdepartmentMapper extends EntityMapper<MsttdepartmentDTO, Msttdepartment> {}
