package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.MsttblockDetailsDTO;

/**
 * Mapper for the entity {@link MsttblockDetails} and its DTO {@link MsttblockDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MsttblockDetailsMapper extends EntityMapper<MsttblockDetailsDTO, MsttblockDetails> {}
