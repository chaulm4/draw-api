package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.MsttblockDTO;

/**
 * Mapper for the entity {@link Msttblock} and its DTO {@link MsttblockDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MsttblockMapper extends EntityMapper<MsttblockDTO, Msttblock> {}
