package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.MsttblockDetails} entity.
 */
@ApiModel(description = "The MsttblockDetail entity.\n@author A true hipster")
public class MsttblockDetailsDTO implements Serializable {

    @NotNull
    @Max(value = 8L)
    private Long id;

    @Max(value = 8L)
    private Long refId;

    @NotNull
    @Size(max = 30)
    private String blockCd;

    @NotNull
    @Size(max = 30)
    private String branchCd;

    @NotNull
    @Max(value = 1)
    private Integer status;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "remarks", required = true)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @ApiModelProperty(value = "regDateTime", required = true)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "regUserId", required = true)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @ApiModelProperty(value = "updDateTime", required = true)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "updUserId", required = true)
    private String updUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getBlockCd() {
        return blockCd;
    }

    public void setBlockCd(String blockCd) {
        this.blockCd = blockCd;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttblockDetailsDTO)) {
            return false;
        }

        MsttblockDetailsDTO msttblockDetailsDTO = (MsttblockDetailsDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, msttblockDetailsDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttblockDetailsDTO{" +
            "id=" + getId() +
            ", refId=" + getRefId() +
            ", blockCd='" + getBlockCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", status=" + getStatus() +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
