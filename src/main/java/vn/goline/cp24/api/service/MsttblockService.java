package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.Msttblock;
import vn.goline.cp24.api.repository.MsttblockRepository;
import vn.goline.cp24.api.service.dto.MsttblockDTO;
import vn.goline.cp24.api.service.mapper.MsttblockMapper;

/**
 * Service Implementation for managing {@link Msttblock}.
 */
@Service
@Transactional
public class MsttblockService {

    private final Logger log = LoggerFactory.getLogger(MsttblockService.class);

    private final MsttblockRepository msttblockRepository;

    private final MsttblockMapper msttblockMapper;

    public MsttblockService(MsttblockRepository msttblockRepository, MsttblockMapper msttblockMapper) {
        this.msttblockRepository = msttblockRepository;
        this.msttblockMapper = msttblockMapper;
    }

    /**
     * Save a msttblock.
     *
     * @param msttblockDTO the entity to save.
     * @return the persisted entity.
     */
    public MsttblockDTO save(MsttblockDTO msttblockDTO) {
        log.debug("Request to save Msttblock : {}", msttblockDTO);
        Msttblock msttblock = msttblockMapper.toEntity(msttblockDTO);
        msttblock = msttblockRepository.save(msttblock);
        return msttblockMapper.toDto(msttblock);
    }

    /**
     * Partially update a msttblock.
     *
     * @param msttblockDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MsttblockDTO> partialUpdate(MsttblockDTO msttblockDTO) {
        log.debug("Request to partially update Msttblock : {}", msttblockDTO);

        return msttblockRepository
            .findById(msttblockDTO.getId())
            .map(existingMsttblock -> {
                msttblockMapper.partialUpdate(existingMsttblock, msttblockDTO);

                return existingMsttblock;
            })
            .map(msttblockRepository::save)
            .map(msttblockMapper::toDto);
    }

    /**
     * Get all the msttblocks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MsttblockDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Msttblocks");
        return msttblockRepository.findAll(pageable).map(msttblockMapper::toDto);
    }

    /**
     * Get one msttblock by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MsttblockDTO> findOne(Long id) {
        log.debug("Request to get Msttblock : {}", id);
        return msttblockRepository.findById(id).map(msttblockMapper::toDto);
    }

    /**
     * Delete the msttblock by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Msttblock : {}", id);
        msttblockRepository.deleteById(id);
    }
}
