package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.Msttdepartment;
import vn.goline.cp24.api.repository.MsttdepartmentRepository;
import vn.goline.cp24.api.service.dto.MsttdepartmentDTO;
import vn.goline.cp24.api.service.mapper.MsttdepartmentMapper;

/**
 * Service Implementation for managing {@link Msttdepartment}.
 */
@Service
@Transactional
public class MsttdepartmentService {

    private final Logger log = LoggerFactory.getLogger(MsttdepartmentService.class);

    private final MsttdepartmentRepository msttdepartmentRepository;

    private final MsttdepartmentMapper msttdepartmentMapper;

    public MsttdepartmentService(MsttdepartmentRepository msttdepartmentRepository, MsttdepartmentMapper msttdepartmentMapper) {
        this.msttdepartmentRepository = msttdepartmentRepository;
        this.msttdepartmentMapper = msttdepartmentMapper;
    }

    /**
     * Save a msttdepartment.
     *
     * @param msttdepartmentDTO the entity to save.
     * @return the persisted entity.
     */
    public MsttdepartmentDTO save(MsttdepartmentDTO msttdepartmentDTO) {
        log.debug("Request to save Msttdepartment : {}", msttdepartmentDTO);
        Msttdepartment msttdepartment = msttdepartmentMapper.toEntity(msttdepartmentDTO);
        msttdepartment = msttdepartmentRepository.save(msttdepartment);
        return msttdepartmentMapper.toDto(msttdepartment);
    }

    /**
     * Partially update a msttdepartment.
     *
     * @param msttdepartmentDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MsttdepartmentDTO> partialUpdate(MsttdepartmentDTO msttdepartmentDTO) {
        log.debug("Request to partially update Msttdepartment : {}", msttdepartmentDTO);

        return msttdepartmentRepository
            .findById(msttdepartmentDTO.getId())
            .map(existingMsttdepartment -> {
                msttdepartmentMapper.partialUpdate(existingMsttdepartment, msttdepartmentDTO);

                return existingMsttdepartment;
            })
            .map(msttdepartmentRepository::save)
            .map(msttdepartmentMapper::toDto);
    }

    /**
     * Get all the msttdepartments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MsttdepartmentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Msttdepartments");
        return msttdepartmentRepository.findAll(pageable).map(msttdepartmentMapper::toDto);
    }

    /**
     * Get one msttdepartment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MsttdepartmentDTO> findOne(Long id) {
        log.debug("Request to get Msttdepartment : {}", id);
        return msttdepartmentRepository.findById(id).map(msttdepartmentMapper::toDto);
    }

    /**
     * Delete the msttdepartment by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Msttdepartment : {}", id);
        msttdepartmentRepository.deleteById(id);
    }
}
