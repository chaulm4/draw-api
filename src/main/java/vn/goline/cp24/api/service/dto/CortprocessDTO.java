package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.Cortprocess} entity.
 */
@ApiModel(description = "The Cortprocess entity.\n@author A true hipster")
public class CortprocessDTO implements Serializable {

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 50)
    @ApiModelProperty(value = "fieldName", required = true)
    private String processCd;

    @Size(max = 50)
    private String refProcessCd;

    @NotNull
    private Integer type;

    @Size(max = 100)
    private String name;

    private Instant lastTime;

    @Size(max = 50)
    private String lastId;

    private Integer allowFlag;

    private Integer counter;

    @NotNull
    private Integer status;

    @NotNull
    private Instant regDateTime;

    @NotNull
    @Size(max = 30)
    private String regUserId;

    @NotNull
    private Instant updDateTime;

    @NotNull
    @Size(max = 30)
    private String updUserId;

    public String getProcessCd() {
        return processCd;
    }

    public void setProcessCd(String processCd) {
        this.processCd = processCd;
    }

    public String getRefProcessCd() {
        return refProcessCd;
    }

    public void setRefProcessCd(String refProcessCd) {
        this.refProcessCd = refProcessCd;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getLastTime() {
        return lastTime;
    }

    public void setLastTime(Instant lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastId() {
        return lastId;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public Integer getAllowFlag() {
        return allowFlag;
    }

    public void setAllowFlag(Integer allowFlag) {
        this.allowFlag = allowFlag;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CortprocessDTO)) {
            return false;
        }

        CortprocessDTO cortprocessDTO = (CortprocessDTO) o;
        if (this.processCd == null) {
            return false;
        }
        return Objects.equals(this.processCd, cortprocessDTO.processCd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.processCd);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CortprocessDTO{" +
            "processCd='" + getProcessCd() + "'" +
            ", refProcessCd='" + getRefProcessCd() + "'" +
            ", type=" + getType() +
            ", name='" + getName() + "'" +
            ", lastTime='" + getLastTime() + "'" +
            ", lastId='" + getLastId() + "'" +
            ", allowFlag=" + getAllowFlag() +
            ", counter=" + getCounter() +
            ", status=" + getStatus() +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
