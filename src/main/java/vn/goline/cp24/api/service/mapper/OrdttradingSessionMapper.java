package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.OrdttradingSessionDTO;

/**
 * Mapper for the entity {@link OrdttradingSession} and its DTO {@link OrdttradingSessionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrdttradingSessionMapper extends EntityMapper<OrdttradingSessionDTO, OrdttradingSession> {}
