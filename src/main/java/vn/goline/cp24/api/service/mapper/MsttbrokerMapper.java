package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.MsttbrokerDTO;

/**
 * Mapper for the entity {@link Msttbroker} and its DTO {@link MsttbrokerDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MsttbrokerMapper extends EntityMapper<MsttbrokerDTO, Msttbroker> {}
