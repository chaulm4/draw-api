package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.Msttblock} entity.
 */
@ApiModel(description = "The Msttblock entity.\n@author A true hipster")
public class MsttblockDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String blockCd;

    @NotNull
    @Size(max = 100)
    private String blockName;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "remarks", required = true)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @ApiModelProperty(value = "regDateTime", required = true)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "regUserId", required = true)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @ApiModelProperty(value = "updDateTime", required = true)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "updUserId", required = true)
    private String updUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBlockCd() {
        return blockCd;
    }

    public void setBlockCd(String blockCd) {
        this.blockCd = blockCd;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttblockDTO)) {
            return false;
        }

        MsttblockDTO msttblockDTO = (MsttblockDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, msttblockDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttblockDTO{" +
            "id=" + getId() +
            ", blockCd='" + getBlockCd() + "'" +
            ", blockName='" + getBlockName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
