package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.Msttdepartment} entity.
 */
@ApiModel(description = "The Msttdepartment entity.\n@author A true hipster")
public class MsttdepartmentDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String departmentCd;

    @NotNull
    @Size(max = 100)
    private String departmentName;

    @NotNull
    @Size(max = 30)
    private String brokerCd;

    @Size(max = 30)
    private String branchCd;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "remarks", required = true)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @ApiModelProperty(value = "regDateTime", required = true)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "regUserId", required = true)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @ApiModelProperty(value = "updDateTime", required = true)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "updUserId", required = true)
    private String updUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentCd() {
        return departmentCd;
    }

    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getBrokerCd() {
        return brokerCd;
    }

    public void setBrokerCd(String brokerCd) {
        this.brokerCd = brokerCd;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttdepartmentDTO)) {
            return false;
        }

        MsttdepartmentDTO msttdepartmentDTO = (MsttdepartmentDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, msttdepartmentDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttdepartmentDTO{" +
            "id=" + getId() +
            ", departmentCd='" + getDepartmentCd() + "'" +
            ", departmentName='" + getDepartmentName() + "'" +
            ", brokerCd='" + getBrokerCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
