package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.Msttbroker;
import vn.goline.cp24.api.repository.MsttbrokerRepository;
import vn.goline.cp24.api.service.dto.MsttbrokerDTO;
import vn.goline.cp24.api.service.mapper.MsttbrokerMapper;

/**
 * Service Implementation for managing {@link Msttbroker}.
 */
@Service
@Transactional
public class MsttbrokerService {

    private final Logger log = LoggerFactory.getLogger(MsttbrokerService.class);

    private final MsttbrokerRepository msttbrokerRepository;

    private final MsttbrokerMapper msttbrokerMapper;

    public MsttbrokerService(MsttbrokerRepository msttbrokerRepository, MsttbrokerMapper msttbrokerMapper) {
        this.msttbrokerRepository = msttbrokerRepository;
        this.msttbrokerMapper = msttbrokerMapper;
    }

    /**
     * Save a msttbroker.
     *
     * @param msttbrokerDTO the entity to save.
     * @return the persisted entity.
     */
    public MsttbrokerDTO save(MsttbrokerDTO msttbrokerDTO) {
        log.debug("Request to save Msttbroker : {}", msttbrokerDTO);
        Msttbroker msttbroker = msttbrokerMapper.toEntity(msttbrokerDTO);
        msttbroker = msttbrokerRepository.save(msttbroker);
        return msttbrokerMapper.toDto(msttbroker);
    }

    /**
     * Partially update a msttbroker.
     *
     * @param msttbrokerDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MsttbrokerDTO> partialUpdate(MsttbrokerDTO msttbrokerDTO) {
        log.debug("Request to partially update Msttbroker : {}", msttbrokerDTO);

        return msttbrokerRepository
            .findById(msttbrokerDTO.getId())
            .map(existingMsttbroker -> {
                msttbrokerMapper.partialUpdate(existingMsttbroker, msttbrokerDTO);

                return existingMsttbroker;
            })
            .map(msttbrokerRepository::save)
            .map(msttbrokerMapper::toDto);
    }

    /**
     * Get all the msttbrokers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MsttbrokerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Msttbrokers");
        return msttbrokerRepository.findAll(pageable).map(msttbrokerMapper::toDto);
    }

    /**
     * Get one msttbroker by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MsttbrokerDTO> findOne(Long id) {
        log.debug("Request to get Msttbroker : {}", id);
        return msttbrokerRepository.findById(id).map(msttbrokerMapper::toDto);
    }

    /**
     * Delete the msttbroker by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Msttbroker : {}", id);
        msttbrokerRepository.deleteById(id);
    }
}
