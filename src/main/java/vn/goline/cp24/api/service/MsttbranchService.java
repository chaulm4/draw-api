package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.Msttbranch;
import vn.goline.cp24.api.repository.MsttbranchRepository;
import vn.goline.cp24.api.service.dto.MsttbranchDTO;
import vn.goline.cp24.api.service.mapper.MsttbranchMapper;

/**
 * Service Implementation for managing {@link Msttbranch}.
 */
@Service
@Transactional
public class MsttbranchService {

    private final Logger log = LoggerFactory.getLogger(MsttbranchService.class);

    private final MsttbranchRepository msttbranchRepository;

    private final MsttbranchMapper msttbranchMapper;

    public MsttbranchService(MsttbranchRepository msttbranchRepository, MsttbranchMapper msttbranchMapper) {
        this.msttbranchRepository = msttbranchRepository;
        this.msttbranchMapper = msttbranchMapper;
    }

    /**
     * Save a msttbranch.
     *
     * @param msttbranchDTO the entity to save.
     * @return the persisted entity.
     */
    public MsttbranchDTO save(MsttbranchDTO msttbranchDTO) {
        log.debug("Request to save Msttbranch : {}", msttbranchDTO);
        Msttbranch msttbranch = msttbranchMapper.toEntity(msttbranchDTO);
        msttbranch = msttbranchRepository.save(msttbranch);
        return msttbranchMapper.toDto(msttbranch);
    }

    /**
     * Partially update a msttbranch.
     *
     * @param msttbranchDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MsttbranchDTO> partialUpdate(MsttbranchDTO msttbranchDTO) {
        log.debug("Request to partially update Msttbranch : {}", msttbranchDTO);

        return msttbranchRepository
            .findById(msttbranchDTO.getId())
            .map(existingMsttbranch -> {
                msttbranchMapper.partialUpdate(existingMsttbranch, msttbranchDTO);

                return existingMsttbranch;
            })
            .map(msttbranchRepository::save)
            .map(msttbranchMapper::toDto);
    }

    /**
     * Get all the msttbranches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MsttbranchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Msttbranches");
        return msttbranchRepository.findAll(pageable).map(msttbranchMapper::toDto);
    }

    /**
     * Get one msttbranch by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MsttbranchDTO> findOne(Long id) {
        log.debug("Request to get Msttbranch : {}", id);
        return msttbranchRepository.findById(id).map(msttbranchMapper::toDto);
    }

    /**
     * Delete the msttbranch by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Msttbranch : {}", id);
        msttbranchRepository.deleteById(id);
    }
}
