package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.OrdttradingSession} entity.
 */
@ApiModel(description = "The OrdttradingSession entity.\n@author A true hipster")
public class OrdttradingSessionDTO implements Serializable {

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 10)
    @ApiModelProperty(value = "fieldName", required = true)
    private String tradingSessionId;

    @Size(max = 20)
    private String tradingSessionSubId;

    private Integer tradDate;

    @NotNull
    @Size(max = 20)
    private String sessionCd;

    @Size(max = 20)
    private String status;

    private Integer tradingFlag;

    private Instant regDateTime;

    @Size(max = 30)
    private String regUserId;

    private Instant updDateTime;

    @Size(max = 30)
    private String updUserId;

    public String getTradingSessionId() {
        return tradingSessionId;
    }

    public void setTradingSessionId(String tradingSessionId) {
        this.tradingSessionId = tradingSessionId;
    }

    public String getTradingSessionSubId() {
        return tradingSessionSubId;
    }

    public void setTradingSessionSubId(String tradingSessionSubId) {
        this.tradingSessionSubId = tradingSessionSubId;
    }

    public Integer getTradDate() {
        return tradDate;
    }

    public void setTradDate(Integer tradDate) {
        this.tradDate = tradDate;
    }

    public String getSessionCd() {
        return sessionCd;
    }

    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTradingFlag() {
        return tradingFlag;
    }

    public void setTradingFlag(Integer tradingFlag) {
        this.tradingFlag = tradingFlag;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdttradingSessionDTO)) {
            return false;
        }

        OrdttradingSessionDTO ordttradingSessionDTO = (OrdttradingSessionDTO) o;
        if (this.tradingSessionId == null) {
            return false;
        }
        return Objects.equals(this.tradingSessionId, ordttradingSessionDTO.tradingSessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.tradingSessionId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdttradingSessionDTO{" +
            "tradingSessionId='" + getTradingSessionId() + "'" +
            ", tradingSessionSubId='" + getTradingSessionSubId() + "'" +
            ", tradDate=" + getTradDate() +
            ", sessionCd='" + getSessionCd() + "'" +
            ", status='" + getStatus() + "'" +
            ", tradingFlag=" + getTradingFlag() +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
