package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.CortprocessDTO;

/**
 * Mapper for the entity {@link Cortprocess} and its DTO {@link CortprocessDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CortprocessMapper extends EntityMapper<CortprocessDTO, Cortprocess> {}
