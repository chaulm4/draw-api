package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.MsttblockDetails;
import vn.goline.cp24.api.repository.MsttblockDetailsRepository;
import vn.goline.cp24.api.service.dto.MsttblockDetailsDTO;
import vn.goline.cp24.api.service.mapper.MsttblockDetailsMapper;

/**
 * Service Implementation for managing {@link MsttblockDetails}.
 */
@Service
@Transactional
public class MsttblockDetailsService {

    private final Logger log = LoggerFactory.getLogger(MsttblockDetailsService.class);

    private final MsttblockDetailsRepository msttblockDetailsRepository;

    private final MsttblockDetailsMapper msttblockDetailsMapper;

    public MsttblockDetailsService(MsttblockDetailsRepository msttblockDetailsRepository, MsttblockDetailsMapper msttblockDetailsMapper) {
        this.msttblockDetailsRepository = msttblockDetailsRepository;
        this.msttblockDetailsMapper = msttblockDetailsMapper;
    }

    /**
     * Save a msttblockDetails.
     *
     * @param msttblockDetailsDTO the entity to save.
     * @return the persisted entity.
     */
    public MsttblockDetailsDTO save(MsttblockDetailsDTO msttblockDetailsDTO) {
        log.debug("Request to save MsttblockDetails : {}", msttblockDetailsDTO);
        MsttblockDetails msttblockDetails = msttblockDetailsMapper.toEntity(msttblockDetailsDTO);
        msttblockDetails = msttblockDetailsRepository.save(msttblockDetails);
        return msttblockDetailsMapper.toDto(msttblockDetails);
    }

    /**
     * Partially update a msttblockDetails.
     *
     * @param msttblockDetailsDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MsttblockDetailsDTO> partialUpdate(MsttblockDetailsDTO msttblockDetailsDTO) {
        log.debug("Request to partially update MsttblockDetails : {}", msttblockDetailsDTO);

        return msttblockDetailsRepository
            .findById(msttblockDetailsDTO.getId())
            .map(existingMsttblockDetails -> {
                msttblockDetailsMapper.partialUpdate(existingMsttblockDetails, msttblockDetailsDTO);

                return existingMsttblockDetails;
            })
            .map(msttblockDetailsRepository::save)
            .map(msttblockDetailsMapper::toDto);
    }

    /**
     * Get all the msttblockDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MsttblockDetailsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MsttblockDetails");
        return msttblockDetailsRepository.findAll(pageable).map(msttblockDetailsMapper::toDto);
    }

    /**
     * Get one msttblockDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MsttblockDetailsDTO> findOne(Long id) {
        log.debug("Request to get MsttblockDetails : {}", id);
        return msttblockDetailsRepository.findById(id).map(msttblockDetailsMapper::toDto);
    }

    /**
     * Delete the msttblockDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MsttblockDetails : {}", id);
        msttblockDetailsRepository.deleteById(id);
    }
}
