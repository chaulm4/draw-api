package vn.goline.cp24.api.service.mapper;

import org.mapstruct.*;
import vn.goline.cp24.api.domain.*;
import vn.goline.cp24.api.service.dto.MsttbranchDTO;

/**
 * Mapper for the entity {@link Msttbranch} and its DTO {@link MsttbranchDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MsttbranchMapper extends EntityMapper<MsttbranchDTO, Msttbranch> {}
