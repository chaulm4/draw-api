package vn.goline.cp24.api.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link vn.goline.cp24.api.domain.Msttbranch} entity.
 */
@ApiModel(description = "The Msttbranch entity.\n@author A true hipster")
public class MsttbranchDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 30)
    private String branchCd;

    @NotNull
    @Size(max = 100)
    private String branchName;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @ApiModelProperty(value = "remarks", required = true)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @ApiModelProperty(value = "regDateTime", required = true)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "regUserId", required = true)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @ApiModelProperty(value = "updDateTime", required = true)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @ApiModelProperty(value = "updUserId", required = true)
    private String updUserId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBranchCd() {
        return branchCd;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return regDateTime;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return regUserId;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return updDateTime;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return updUserId;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttbranchDTO)) {
            return false;
        }

        MsttbranchDTO msttbranchDTO = (MsttbranchDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, msttbranchDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttbranchDTO{" +
            "id=" + getId() +
            ", branchCd='" + getBranchCd() + "'" +
            ", branchName='" + getBranchName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
