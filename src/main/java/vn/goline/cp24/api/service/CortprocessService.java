package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.Cortprocess;
import vn.goline.cp24.api.repository.CortprocessRepository;
import vn.goline.cp24.api.service.dto.CortprocessDTO;
import vn.goline.cp24.api.service.mapper.CortprocessMapper;

/**
 * Service Implementation for managing {@link Cortprocess}.
 */
@Service
@Transactional
public class CortprocessService {

    private final Logger log = LoggerFactory.getLogger(CortprocessService.class);

    private final CortprocessRepository cortprocessRepository;

    private final CortprocessMapper cortprocessMapper;

    public CortprocessService(CortprocessRepository cortprocessRepository, CortprocessMapper cortprocessMapper) {
        this.cortprocessRepository = cortprocessRepository;
        this.cortprocessMapper = cortprocessMapper;
    }

    /**
     * Save a cortprocess.
     *
     * @param cortprocessDTO the entity to save.
     * @return the persisted entity.
     */
    public CortprocessDTO save(CortprocessDTO cortprocessDTO) {
        log.debug("Request to save Cortprocess : {}", cortprocessDTO);
        Cortprocess cortprocess = cortprocessMapper.toEntity(cortprocessDTO);
        cortprocess = cortprocessRepository.save(cortprocess);
        return cortprocessMapper.toDto(cortprocess);
    }

    /**
     * Partially update a cortprocess.
     *
     * @param cortprocessDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CortprocessDTO> partialUpdate(CortprocessDTO cortprocessDTO) {
        log.debug("Request to partially update Cortprocess : {}", cortprocessDTO);

        return cortprocessRepository
            .findById(cortprocessDTO.getProcessCd())
            .map(existingCortprocess -> {
                cortprocessMapper.partialUpdate(existingCortprocess, cortprocessDTO);

                return existingCortprocess;
            })
            .map(cortprocessRepository::save)
            .map(cortprocessMapper::toDto);
    }

    /**
     * Get all the cortprocesses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CortprocessDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Cortprocesses");
        return cortprocessRepository.findAll(pageable).map(cortprocessMapper::toDto);
    }

    /**
     * Get one cortprocess by processCd.
     *
     * @param processCd the processCd of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CortprocessDTO> findOne(String processCd) {
        log.debug("Request to get Cortprocess : {}", processCd);
        return cortprocessRepository.findById(processCd).map(cortprocessMapper::toDto);
    }

    /**
     * Delete the cortprocess by processCd.
     *
     * @param processCd the processCd of the entity.
     */
    public void delete(String processCd) {
        log.debug("Request to delete Cortprocess : {}", processCd);
        cortprocessRepository.deleteById(processCd);
    }
}
