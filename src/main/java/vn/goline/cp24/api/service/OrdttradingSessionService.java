package vn.goline.cp24.api.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.goline.cp24.api.domain.OrdttradingSession;
import vn.goline.cp24.api.repository.OrdttradingSessionRepository;
import vn.goline.cp24.api.service.dto.OrdttradingSessionDTO;
import vn.goline.cp24.api.service.mapper.OrdttradingSessionMapper;

/**
 * Service Implementation for managing {@link OrdttradingSession}.
 */
@Service
@Transactional
public class OrdttradingSessionService {

    private final Logger log = LoggerFactory.getLogger(OrdttradingSessionService.class);

    private final OrdttradingSessionRepository ordttradingSessionRepository;

    private final OrdttradingSessionMapper ordttradingSessionMapper;

    public OrdttradingSessionService(
        OrdttradingSessionRepository ordttradingSessionRepository,
        OrdttradingSessionMapper ordttradingSessionMapper
    ) {
        this.ordttradingSessionRepository = ordttradingSessionRepository;
        this.ordttradingSessionMapper = ordttradingSessionMapper;
    }

    /**
     * Save a ordttradingSession.
     *
     * @param ordttradingSessionDTO the entity to save.
     * @return the persisted entity.
     */
    public OrdttradingSessionDTO save(OrdttradingSessionDTO ordttradingSessionDTO) {
        log.debug("Request to save OrdttradingSession : {}", ordttradingSessionDTO);
        OrdttradingSession ordttradingSession = ordttradingSessionMapper.toEntity(ordttradingSessionDTO);
        ordttradingSession = ordttradingSessionRepository.save(ordttradingSession);
        return ordttradingSessionMapper.toDto(ordttradingSession);
    }

    /**
     * Partially update a ordttradingSession.
     *
     * @param ordttradingSessionDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OrdttradingSessionDTO> partialUpdate(OrdttradingSessionDTO ordttradingSessionDTO) {
        log.debug("Request to partially update OrdttradingSession : {}", ordttradingSessionDTO);

        return ordttradingSessionRepository
            .findById(ordttradingSessionDTO.getTradingSessionId())
            .map(existingOrdttradingSession -> {
                ordttradingSessionMapper.partialUpdate(existingOrdttradingSession, ordttradingSessionDTO);

                return existingOrdttradingSession;
            })
            .map(ordttradingSessionRepository::save)
            .map(ordttradingSessionMapper::toDto);
    }

    /**
     * Get all the ordttradingSessions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrdttradingSessionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrdttradingSessions");
        return ordttradingSessionRepository.findAll(pageable).map(ordttradingSessionMapper::toDto);
    }

    /**
     * Get one ordttradingSession by tradingSessionId.
     *
     * @param tradingSessionId the tradingSessionId of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrdttradingSessionDTO> findOne(String tradingSessionId) {
        log.debug("Request to get OrdttradingSession : {}", tradingSessionId);
        return ordttradingSessionRepository.findById(tradingSessionId).map(ordttradingSessionMapper::toDto);
    }

    /**
     * Delete the ordttradingSession by tradingSessionId.
     *
     * @param tradingSessionId the tradingSessionId of the entity.
     */
    public void delete(String tradingSessionId) {
        log.debug("Request to delete OrdttradingSession : {}", tradingSessionId);
        ordttradingSessionRepository.deleteById(tradingSessionId);
    }
}
