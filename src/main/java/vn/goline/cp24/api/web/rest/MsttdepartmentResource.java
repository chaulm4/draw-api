package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.MsttdepartmentRepository;
import vn.goline.cp24.api.service.MsttdepartmentService;
import vn.goline.cp24.api.service.dto.MsttdepartmentDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.Msttdepartment}.
 */
@RestController
@RequestMapping("/api")
public class MsttdepartmentResource {

    private final Logger log = LoggerFactory.getLogger(MsttdepartmentResource.class);

    private static final String ENTITY_NAME = "msttdepartment";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MsttdepartmentService msttdepartmentService;

    private final MsttdepartmentRepository msttdepartmentRepository;

    public MsttdepartmentResource(MsttdepartmentService msttdepartmentService, MsttdepartmentRepository msttdepartmentRepository) {
        this.msttdepartmentService = msttdepartmentService;
        this.msttdepartmentRepository = msttdepartmentRepository;
    }

    /**
     * {@code POST  /msttdepartments} : Create a new msttdepartment.
     *
     * @param msttdepartmentDTO the msttdepartmentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new msttdepartmentDTO, or with status {@code 400 (Bad Request)} if the msttdepartment has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/msttdepartments")
    public ResponseEntity<MsttdepartmentDTO> createMsttdepartment(@Valid @RequestBody MsttdepartmentDTO msttdepartmentDTO)
        throws URISyntaxException {
        log.debug("REST request to save Msttdepartment : {}", msttdepartmentDTO);
        if (msttdepartmentDTO.getId() != null) {
            throw new BadRequestAlertException("A new msttdepartment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsttdepartmentDTO result = msttdepartmentService.save(msttdepartmentDTO);
        return ResponseEntity
            .created(new URI("/api/msttdepartments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /msttdepartments/:id} : Updates an existing msttdepartment.
     *
     * @param id the id of the msttdepartmentDTO to save.
     * @param msttdepartmentDTO the msttdepartmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttdepartmentDTO,
     * or with status {@code 400 (Bad Request)} if the msttdepartmentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the msttdepartmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/msttdepartments/{id}")
    public ResponseEntity<MsttdepartmentDTO> updateMsttdepartment(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MsttdepartmentDTO msttdepartmentDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Msttdepartment : {}, {}", id, msttdepartmentDTO);
        if (msttdepartmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttdepartmentDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttdepartmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MsttdepartmentDTO result = msttdepartmentService.save(msttdepartmentDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttdepartmentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /msttdepartments/:id} : Partial updates given fields of an existing msttdepartment, field will ignore if it is null
     *
     * @param id the id of the msttdepartmentDTO to save.
     * @param msttdepartmentDTO the msttdepartmentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttdepartmentDTO,
     * or with status {@code 400 (Bad Request)} if the msttdepartmentDTO is not valid,
     * or with status {@code 404 (Not Found)} if the msttdepartmentDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the msttdepartmentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/msttdepartments/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MsttdepartmentDTO> partialUpdateMsttdepartment(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MsttdepartmentDTO msttdepartmentDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Msttdepartment partially : {}, {}", id, msttdepartmentDTO);
        if (msttdepartmentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttdepartmentDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttdepartmentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MsttdepartmentDTO> result = msttdepartmentService.partialUpdate(msttdepartmentDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttdepartmentDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /msttdepartments} : get all the msttdepartments.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of msttdepartments in body.
     */
    @GetMapping("/msttdepartments")
    public ResponseEntity<List<MsttdepartmentDTO>> getAllMsttdepartments(Pageable pageable) {
        log.debug("REST request to get a page of Msttdepartments");
        Page<MsttdepartmentDTO> page = msttdepartmentService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /msttdepartments/:id} : get the "id" msttdepartment.
     *
     * @param id the id of the msttdepartmentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the msttdepartmentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/msttdepartments/{id}")
    public ResponseEntity<MsttdepartmentDTO> getMsttdepartment(@PathVariable Long id) {
        log.debug("REST request to get Msttdepartment : {}", id);
        Optional<MsttdepartmentDTO> msttdepartmentDTO = msttdepartmentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msttdepartmentDTO);
    }

    /**
     * {@code DELETE  /msttdepartments/:id} : delete the "id" msttdepartment.
     *
     * @param id the id of the msttdepartmentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/msttdepartments/{id}")
    public ResponseEntity<Void> deleteMsttdepartment(@PathVariable Long id) {
        log.debug("REST request to delete Msttdepartment : {}", id);
        msttdepartmentService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
