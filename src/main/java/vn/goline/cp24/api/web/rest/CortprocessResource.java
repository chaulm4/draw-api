package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.CortprocessRepository;
import vn.goline.cp24.api.service.CortprocessService;
import vn.goline.cp24.api.service.dto.CortprocessDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.Cortprocess}.
 */
@RestController
@RequestMapping("/api")
public class CortprocessResource {

    private final Logger log = LoggerFactory.getLogger(CortprocessResource.class);

    private static final String ENTITY_NAME = "cortprocess";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CortprocessService cortprocessService;

    private final CortprocessRepository cortprocessRepository;

    public CortprocessResource(CortprocessService cortprocessService, CortprocessRepository cortprocessRepository) {
        this.cortprocessService = cortprocessService;
        this.cortprocessRepository = cortprocessRepository;
    }

    /**
     * {@code POST  /cortprocesses} : Create a new cortprocess.
     *
     * @param cortprocessDTO the cortprocessDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cortprocessDTO, or with status {@code 400 (Bad Request)} if the cortprocess has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cortprocesses")
    public ResponseEntity<CortprocessDTO> createCortprocess(@Valid @RequestBody CortprocessDTO cortprocessDTO) throws URISyntaxException {
        log.debug("REST request to save Cortprocess : {}", cortprocessDTO);
        if (cortprocessDTO.getProcessCd() != null) {
            throw new BadRequestAlertException("A new cortprocess cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CortprocessDTO result = cortprocessService.save(cortprocessDTO);
        return ResponseEntity
            .created(new URI("/api/cortprocesses/" + result.getProcessCd()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getProcessCd()))
            .body(result);
    }

    /**
     * {@code PUT  /cortprocesses/:processCd} : Updates an existing cortprocess.
     *
     * @param processCd the processCd of the cortprocessDTO to save.
     * @param cortprocessDTO the cortprocessDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cortprocessDTO,
     * or with status {@code 400 (Bad Request)} if the cortprocessDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cortprocessDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cortprocesses/{processCd}")
    public ResponseEntity<CortprocessDTO> updateCortprocess(
        @PathVariable(value = "processCd", required = false) final String processCd,
        @Valid @RequestBody CortprocessDTO cortprocessDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Cortprocess : {}, {}", processCd, cortprocessDTO);
        if (cortprocessDTO.getProcessCd() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(processCd, cortprocessDTO.getProcessCd())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cortprocessRepository.existsById(processCd)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CortprocessDTO result = cortprocessService.save(cortprocessDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cortprocessDTO.getProcessCd()))
            .body(result);
    }

    /**
     * {@code PATCH  /cortprocesses/:processCd} : Partial updates given fields of an existing cortprocess, field will ignore if it is null
     *
     * @param processCd the processCd of the cortprocessDTO to save.
     * @param cortprocessDTO the cortprocessDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cortprocessDTO,
     * or with status {@code 400 (Bad Request)} if the cortprocessDTO is not valid,
     * or with status {@code 404 (Not Found)} if the cortprocessDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the cortprocessDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/cortprocesses/{processCd}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CortprocessDTO> partialUpdateCortprocess(
        @PathVariable(value = "processCd", required = false) final String processCd,
        @NotNull @RequestBody CortprocessDTO cortprocessDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Cortprocess partially : {}, {}", processCd, cortprocessDTO);
        if (cortprocessDTO.getProcessCd() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(processCd, cortprocessDTO.getProcessCd())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cortprocessRepository.existsById(processCd)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CortprocessDTO> result = cortprocessService.partialUpdate(cortprocessDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cortprocessDTO.getProcessCd())
        );
    }

    /**
     * {@code GET  /cortprocesses} : get all the cortprocesses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cortprocesses in body.
     */
    @GetMapping("/cortprocesses")
    public ResponseEntity<List<CortprocessDTO>> getAllCortprocesses(Pageable pageable) {
        log.debug("REST request to get a page of Cortprocesses");
        Page<CortprocessDTO> page = cortprocessService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cortprocesses/:processCd} : get the "processCd" cortprocess.
     *
     * @param processCd the processCd of the cortprocessDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cortprocessDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cortprocesses/{processCd}")
    public ResponseEntity<CortprocessDTO> getCortprocess(@PathVariable String processCd) {
        log.debug("REST request to get Cortprocess : {}", processCd);
        Optional<CortprocessDTO> cortprocessDTO = cortprocessService.findOne(processCd);
        return ResponseUtil.wrapOrNotFound(cortprocessDTO);
    }

    /**
     * {@code DELETE  /cortprocesses/:processCd} : delete the "processCd" cortprocess.
     *
     * @param processCd the processCd of the cortprocessDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cortprocesses/{processCd}")
    public ResponseEntity<Void> deleteCortprocess(@PathVariable String processCd) {
        log.debug("REST request to delete Cortprocess : {}", processCd);
        cortprocessService.delete(processCd);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, processCd))
            .build();
    }
}
