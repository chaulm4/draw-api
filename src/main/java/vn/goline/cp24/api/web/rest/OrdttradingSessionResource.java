package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.OrdttradingSessionRepository;
import vn.goline.cp24.api.service.OrdttradingSessionService;
import vn.goline.cp24.api.service.dto.OrdttradingSessionDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.OrdttradingSession}.
 */
@RestController
@RequestMapping("/api")
public class OrdttradingSessionResource {

    private final Logger log = LoggerFactory.getLogger(OrdttradingSessionResource.class);

    private static final String ENTITY_NAME = "ordttradingSession";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrdttradingSessionService ordttradingSessionService;

    private final OrdttradingSessionRepository ordttradingSessionRepository;

    public OrdttradingSessionResource(
        OrdttradingSessionService ordttradingSessionService,
        OrdttradingSessionRepository ordttradingSessionRepository
    ) {
        this.ordttradingSessionService = ordttradingSessionService;
        this.ordttradingSessionRepository = ordttradingSessionRepository;
    }

    /**
     * {@code POST  /ordttrading-sessions} : Create a new ordttradingSession.
     *
     * @param ordttradingSessionDTO the ordttradingSessionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ordttradingSessionDTO, or with status {@code 400 (Bad Request)} if the ordttradingSession has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ordttrading-sessions")
    public ResponseEntity<OrdttradingSessionDTO> createOrdttradingSession(@Valid @RequestBody OrdttradingSessionDTO ordttradingSessionDTO)
        throws URISyntaxException {
        log.debug("REST request to save OrdttradingSession : {}", ordttradingSessionDTO);
        if (ordttradingSessionDTO.getTradingSessionId() != null) {
            throw new BadRequestAlertException("A new ordttradingSession cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrdttradingSessionDTO result = ordttradingSessionService.save(ordttradingSessionDTO);
        return ResponseEntity
            .created(new URI("/api/ordttrading-sessions/" + result.getTradingSessionId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getTradingSessionId()))
            .body(result);
    }

    /**
     * {@code PUT  /ordttrading-sessions/:tradingSessionId} : Updates an existing ordttradingSession.
     *
     * @param tradingSessionId the tradingSessionId of the ordttradingSessionDTO to save.
     * @param ordttradingSessionDTO the ordttradingSessionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ordttradingSessionDTO,
     * or with status {@code 400 (Bad Request)} if the ordttradingSessionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ordttradingSessionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ordttrading-sessions/{tradingSessionId}")
    public ResponseEntity<OrdttradingSessionDTO> updateOrdttradingSession(
        @PathVariable(value = "tradingSessionId", required = false) final String tradingSessionId,
        @Valid @RequestBody OrdttradingSessionDTO ordttradingSessionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update OrdttradingSession : {}, {}", tradingSessionId, ordttradingSessionDTO);
        if (ordttradingSessionDTO.getTradingSessionId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(tradingSessionId, ordttradingSessionDTO.getTradingSessionId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ordttradingSessionRepository.existsById(tradingSessionId)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrdttradingSessionDTO result = ordttradingSessionService.save(ordttradingSessionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ordttradingSessionDTO.getTradingSessionId()))
            .body(result);
    }

    /**
     * {@code PATCH  /ordttrading-sessions/:tradingSessionId} : Partial updates given fields of an existing ordttradingSession, field will ignore if it is null
     *
     * @param tradingSessionId the tradingSessionId of the ordttradingSessionDTO to save.
     * @param ordttradingSessionDTO the ordttradingSessionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ordttradingSessionDTO,
     * or with status {@code 400 (Bad Request)} if the ordttradingSessionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the ordttradingSessionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the ordttradingSessionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/ordttrading-sessions/{tradingSessionId}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrdttradingSessionDTO> partialUpdateOrdttradingSession(
        @PathVariable(value = "tradingSessionId", required = false) final String tradingSessionId,
        @NotNull @RequestBody OrdttradingSessionDTO ordttradingSessionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrdttradingSession partially : {}, {}", tradingSessionId, ordttradingSessionDTO);
        if (ordttradingSessionDTO.getTradingSessionId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(tradingSessionId, ordttradingSessionDTO.getTradingSessionId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ordttradingSessionRepository.existsById(tradingSessionId)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrdttradingSessionDTO> result = ordttradingSessionService.partialUpdate(ordttradingSessionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ordttradingSessionDTO.getTradingSessionId())
        );
    }

    /**
     * {@code GET  /ordttrading-sessions} : get all the ordttradingSessions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ordttradingSessions in body.
     */
    @GetMapping("/ordttrading-sessions")
    public ResponseEntity<List<OrdttradingSessionDTO>> getAllOrdttradingSessions(Pageable pageable) {
        log.debug("REST request to get a page of OrdttradingSessions");
        Page<OrdttradingSessionDTO> page = ordttradingSessionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ordttrading-sessions/:tradingSessionId} : get the "id" ordttradingSession.
     *
     * @param tradingSessionId the tradingSessionId of the ordttradingSessionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ordttradingSessionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ordttrading-sessions/{tradingSessionId}")
    public ResponseEntity<OrdttradingSessionDTO> getOrdttradingSession(@PathVariable String tradingSessionId) {
        log.debug("REST request to get OrdttradingSession : {}", tradingSessionId);
        Optional<OrdttradingSessionDTO> ordttradingSessionDTO = ordttradingSessionService.findOne(tradingSessionId);
        return ResponseUtil.wrapOrNotFound(ordttradingSessionDTO);
    }

    /**
     * {@code DELETE  /ordttrading-sessions/:tradingSessionId} : delete the "tradingSessionId" ordttradingSession.
     *
     * @param tradingSessionId the tradingSessionId of the ordttradingSessionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ordttrading-sessions/{tradingSessionId}")
    public ResponseEntity<Void> deleteOrdttradingSession(@PathVariable String tradingSessionId) {
        log.debug("REST request to delete OrdttradingSession : {}", tradingSessionId);
        ordttradingSessionService.delete(tradingSessionId);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, tradingSessionId))
            .build();
    }
}
