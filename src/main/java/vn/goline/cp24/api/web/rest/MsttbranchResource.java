package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.MsttbranchRepository;
import vn.goline.cp24.api.service.MsttbranchService;
import vn.goline.cp24.api.service.dto.MsttbranchDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.Msttbranch}.
 */
@RestController
@RequestMapping("/api")
public class MsttbranchResource {

    private final Logger log = LoggerFactory.getLogger(MsttbranchResource.class);

    private static final String ENTITY_NAME = "msttbranch";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MsttbranchService msttbranchService;

    private final MsttbranchRepository msttbranchRepository;

    public MsttbranchResource(MsttbranchService msttbranchService, MsttbranchRepository msttbranchRepository) {
        this.msttbranchService = msttbranchService;
        this.msttbranchRepository = msttbranchRepository;
    }

    /**
     * {@code POST  /msttbranches} : Create a new msttbranch.
     *
     * @param msttbranchDTO the msttbranchDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new msttbranchDTO, or with status {@code 400 (Bad Request)} if the msttbranch has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/msttbranches")
    public ResponseEntity<MsttbranchDTO> createMsttbranch(@Valid @RequestBody MsttbranchDTO msttbranchDTO) throws URISyntaxException {
        log.debug("REST request to save Msttbranch : {}", msttbranchDTO);
        if (msttbranchDTO.getId() != null) {
            throw new BadRequestAlertException("A new msttbranch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsttbranchDTO result = msttbranchService.save(msttbranchDTO);
        return ResponseEntity
            .created(new URI("/api/msttbranches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /msttbranches/:id} : Updates an existing msttbranch.
     *
     * @param id the id of the msttbranchDTO to save.
     * @param msttbranchDTO the msttbranchDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttbranchDTO,
     * or with status {@code 400 (Bad Request)} if the msttbranchDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the msttbranchDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/msttbranches/{id}")
    public ResponseEntity<MsttbranchDTO> updateMsttbranch(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MsttbranchDTO msttbranchDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Msttbranch : {}, {}", id, msttbranchDTO);
        if (msttbranchDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttbranchDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttbranchRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MsttbranchDTO result = msttbranchService.save(msttbranchDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttbranchDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /msttbranches/:id} : Partial updates given fields of an existing msttbranch, field will ignore if it is null
     *
     * @param id the id of the msttbranchDTO to save.
     * @param msttbranchDTO the msttbranchDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttbranchDTO,
     * or with status {@code 400 (Bad Request)} if the msttbranchDTO is not valid,
     * or with status {@code 404 (Not Found)} if the msttbranchDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the msttbranchDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/msttbranches/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MsttbranchDTO> partialUpdateMsttbranch(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MsttbranchDTO msttbranchDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Msttbranch partially : {}, {}", id, msttbranchDTO);
        if (msttbranchDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttbranchDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttbranchRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MsttbranchDTO> result = msttbranchService.partialUpdate(msttbranchDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttbranchDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /msttbranches} : get all the msttbranches.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of msttbranches in body.
     */
    @GetMapping("/msttbranches")
    public ResponseEntity<List<MsttbranchDTO>> getAllMsttbranches(Pageable pageable) {
        log.debug("REST request to get a page of Msttbranches");
        Page<MsttbranchDTO> page = msttbranchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /msttbranches/:id} : get the "id" msttbranch.
     *
     * @param id the id of the msttbranchDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the msttbranchDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/msttbranches/{id}")
    public ResponseEntity<MsttbranchDTO> getMsttbranch(@PathVariable Long id) {
        log.debug("REST request to get Msttbranch : {}", id);
        Optional<MsttbranchDTO> msttbranchDTO = msttbranchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msttbranchDTO);
    }

    /**
     * {@code DELETE  /msttbranches/:id} : delete the "id" msttbranch.
     *
     * @param id the id of the msttbranchDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/msttbranches/{id}")
    public ResponseEntity<Void> deleteMsttbranch(@PathVariable Long id) {
        log.debug("REST request to delete Msttbranch : {}", id);
        msttbranchService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
