package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.MsttblockDetailsRepository;
import vn.goline.cp24.api.service.MsttblockDetailsService;
import vn.goline.cp24.api.service.dto.MsttblockDetailsDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.MsttblockDetails}.
 */
@RestController
@RequestMapping("/api")
public class MsttblockDetailsResource {

    private final Logger log = LoggerFactory.getLogger(MsttblockDetailsResource.class);

    private static final String ENTITY_NAME = "msttblockDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MsttblockDetailsService msttblockDetailsService;

    private final MsttblockDetailsRepository msttblockDetailsRepository;

    public MsttblockDetailsResource(
        MsttblockDetailsService msttblockDetailsService,
        MsttblockDetailsRepository msttblockDetailsRepository
    ) {
        this.msttblockDetailsService = msttblockDetailsService;
        this.msttblockDetailsRepository = msttblockDetailsRepository;
    }

    /**
     * {@code POST  /msttblock-details} : Create a new msttblockDetails.
     *
     * @param msttblockDetailsDTO the msttblockDetailsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new msttblockDetailsDTO, or with status {@code 400 (Bad Request)} if the msttblockDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/msttblock-details")
    public ResponseEntity<MsttblockDetailsDTO> createMsttblockDetails(@Valid @RequestBody MsttblockDetailsDTO msttblockDetailsDTO)
        throws URISyntaxException {
        log.debug("REST request to save MsttblockDetails : {}", msttblockDetailsDTO);
        if (msttblockDetailsDTO.getId() != null) {
            throw new BadRequestAlertException("A new msttblockDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsttblockDetailsDTO result = msttblockDetailsService.save(msttblockDetailsDTO);
        return ResponseEntity
            .created(new URI("/api/msttblock-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /msttblock-details/:id} : Updates an existing msttblockDetails.
     *
     * @param id the id of the msttblockDetailsDTO to save.
     * @param msttblockDetailsDTO the msttblockDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttblockDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the msttblockDetailsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the msttblockDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/msttblock-details/{id}")
    public ResponseEntity<MsttblockDetailsDTO> updateMsttblockDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MsttblockDetailsDTO msttblockDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MsttblockDetails : {}, {}", id, msttblockDetailsDTO);
        if (msttblockDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttblockDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttblockDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MsttblockDetailsDTO result = msttblockDetailsService.save(msttblockDetailsDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttblockDetailsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /msttblock-details/:id} : Partial updates given fields of an existing msttblockDetails, field will ignore if it is null
     *
     * @param id the id of the msttblockDetailsDTO to save.
     * @param msttblockDetailsDTO the msttblockDetailsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttblockDetailsDTO,
     * or with status {@code 400 (Bad Request)} if the msttblockDetailsDTO is not valid,
     * or with status {@code 404 (Not Found)} if the msttblockDetailsDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the msttblockDetailsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/msttblock-details/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MsttblockDetailsDTO> partialUpdateMsttblockDetails(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MsttblockDetailsDTO msttblockDetailsDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update MsttblockDetails partially : {}, {}", id, msttblockDetailsDTO);
        if (msttblockDetailsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttblockDetailsDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttblockDetailsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MsttblockDetailsDTO> result = msttblockDetailsService.partialUpdate(msttblockDetailsDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttblockDetailsDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /msttblock-details} : get all the msttblockDetails.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of msttblockDetails in body.
     */
    @GetMapping("/msttblock-details")
    public ResponseEntity<List<MsttblockDetailsDTO>> getAllMsttblockDetails(Pageable pageable) {
        log.debug("REST request to get a page of MsttblockDetails");
        Page<MsttblockDetailsDTO> page = msttblockDetailsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /msttblock-details/:id} : get the "id" msttblockDetails.
     *
     * @param id the id of the msttblockDetailsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the msttblockDetailsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/msttblock-details/{id}")
    public ResponseEntity<MsttblockDetailsDTO> getMsttblockDetails(@PathVariable Long id) {
        log.debug("REST request to get MsttblockDetails : {}", id);
        Optional<MsttblockDetailsDTO> msttblockDetailsDTO = msttblockDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msttblockDetailsDTO);
    }

    /**
     * {@code DELETE  /msttblock-details/:id} : delete the "id" msttblockDetails.
     *
     * @param id the id of the msttblockDetailsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/msttblock-details/{id}")
    public ResponseEntity<Void> deleteMsttblockDetails(@PathVariable Long id) {
        log.debug("REST request to delete MsttblockDetails : {}", id);
        msttblockDetailsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
