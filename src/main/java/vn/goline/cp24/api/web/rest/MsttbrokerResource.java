package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.MsttbrokerRepository;
import vn.goline.cp24.api.service.MsttbrokerService;
import vn.goline.cp24.api.service.dto.MsttbrokerDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.Msttbroker}.
 */
@RestController
@RequestMapping("/api")
public class MsttbrokerResource {

    private final Logger log = LoggerFactory.getLogger(MsttbrokerResource.class);

    private static final String ENTITY_NAME = "msttbroker";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MsttbrokerService msttbrokerService;

    private final MsttbrokerRepository msttbrokerRepository;

    public MsttbrokerResource(MsttbrokerService msttbrokerService, MsttbrokerRepository msttbrokerRepository) {
        this.msttbrokerService = msttbrokerService;
        this.msttbrokerRepository = msttbrokerRepository;
    }

    /**
     * {@code POST  /msttbrokers} : Create a new msttbroker.
     *
     * @param msttbrokerDTO the msttbrokerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new msttbrokerDTO, or with status {@code 400 (Bad Request)} if the msttbroker has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/msttbrokers")
    public ResponseEntity<MsttbrokerDTO> createMsttbroker(@Valid @RequestBody MsttbrokerDTO msttbrokerDTO) throws URISyntaxException {
        log.debug("REST request to save Msttbroker : {}", msttbrokerDTO);
        if (msttbrokerDTO.getId() != null) {
            throw new BadRequestAlertException("A new msttbroker cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsttbrokerDTO result = msttbrokerService.save(msttbrokerDTO);
        return ResponseEntity
            .created(new URI("/api/msttbrokers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /msttbrokers/:id} : Updates an existing msttbroker.
     *
     * @param id the id of the msttbrokerDTO to save.
     * @param msttbrokerDTO the msttbrokerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttbrokerDTO,
     * or with status {@code 400 (Bad Request)} if the msttbrokerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the msttbrokerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/msttbrokers/{id}")
    public ResponseEntity<MsttbrokerDTO> updateMsttbroker(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MsttbrokerDTO msttbrokerDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Msttbroker : {}, {}", id, msttbrokerDTO);
        if (msttbrokerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttbrokerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttbrokerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MsttbrokerDTO result = msttbrokerService.save(msttbrokerDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttbrokerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /msttbrokers/:id} : Partial updates given fields of an existing msttbroker, field will ignore if it is null
     *
     * @param id the id of the msttbrokerDTO to save.
     * @param msttbrokerDTO the msttbrokerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttbrokerDTO,
     * or with status {@code 400 (Bad Request)} if the msttbrokerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the msttbrokerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the msttbrokerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/msttbrokers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MsttbrokerDTO> partialUpdateMsttbroker(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MsttbrokerDTO msttbrokerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Msttbroker partially : {}, {}", id, msttbrokerDTO);
        if (msttbrokerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttbrokerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttbrokerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MsttbrokerDTO> result = msttbrokerService.partialUpdate(msttbrokerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttbrokerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /msttbrokers} : get all the msttbrokers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of msttbrokers in body.
     */
    @GetMapping("/msttbrokers")
    public ResponseEntity<List<MsttbrokerDTO>> getAllMsttbrokers(Pageable pageable) {
        log.debug("REST request to get a page of Msttbrokers");
        Page<MsttbrokerDTO> page = msttbrokerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /msttbrokers/:id} : get the "id" msttbroker.
     *
     * @param id the id of the msttbrokerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the msttbrokerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/msttbrokers/{id}")
    public ResponseEntity<MsttbrokerDTO> getMsttbroker(@PathVariable Long id) {
        log.debug("REST request to get Msttbroker : {}", id);
        Optional<MsttbrokerDTO> msttbrokerDTO = msttbrokerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msttbrokerDTO);
    }

    /**
     * {@code DELETE  /msttbrokers/:id} : delete the "id" msttbroker.
     *
     * @param id the id of the msttbrokerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/msttbrokers/{id}")
    public ResponseEntity<Void> deleteMsttbroker(@PathVariable Long id) {
        log.debug("REST request to delete Msttbroker : {}", id);
        msttbrokerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
