package vn.goline.cp24.api.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;
import vn.goline.cp24.api.repository.MsttblockRepository;
import vn.goline.cp24.api.service.MsttblockService;
import vn.goline.cp24.api.service.dto.MsttblockDTO;
import vn.goline.cp24.api.web.rest.errors.BadRequestAlertException;

/**
 * REST controller for managing {@link vn.goline.cp24.api.domain.Msttblock}.
 */
@RestController
@RequestMapping("/api")
public class MsttblockResource {

    private final Logger log = LoggerFactory.getLogger(MsttblockResource.class);

    private static final String ENTITY_NAME = "msttblock";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MsttblockService msttblockService;

    private final MsttblockRepository msttblockRepository;

    public MsttblockResource(MsttblockService msttblockService, MsttblockRepository msttblockRepository) {
        this.msttblockService = msttblockService;
        this.msttblockRepository = msttblockRepository;
    }

    /**
     * {@code POST  /msttblocks} : Create a new msttblock.
     *
     * @param msttblockDTO the msttblockDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new msttblockDTO, or with status {@code 400 (Bad Request)} if the msttblock has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/msttblocks")
    public ResponseEntity<MsttblockDTO> createMsttblock(@Valid @RequestBody MsttblockDTO msttblockDTO) throws URISyntaxException {
        log.debug("REST request to save Msttblock : {}", msttblockDTO);
        if (msttblockDTO.getId() != null) {
            throw new BadRequestAlertException("A new msttblock cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsttblockDTO result = msttblockService.save(msttblockDTO);
        return ResponseEntity
            .created(new URI("/api/msttblocks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /msttblocks/:id} : Updates an existing msttblock.
     *
     * @param id the id of the msttblockDTO to save.
     * @param msttblockDTO the msttblockDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttblockDTO,
     * or with status {@code 400 (Bad Request)} if the msttblockDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the msttblockDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/msttblocks/{id}")
    public ResponseEntity<MsttblockDTO> updateMsttblock(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MsttblockDTO msttblockDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Msttblock : {}, {}", id, msttblockDTO);
        if (msttblockDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttblockDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttblockRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MsttblockDTO result = msttblockService.save(msttblockDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttblockDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /msttblocks/:id} : Partial updates given fields of an existing msttblock, field will ignore if it is null
     *
     * @param id the id of the msttblockDTO to save.
     * @param msttblockDTO the msttblockDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated msttblockDTO,
     * or with status {@code 400 (Bad Request)} if the msttblockDTO is not valid,
     * or with status {@code 404 (Not Found)} if the msttblockDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the msttblockDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/msttblocks/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MsttblockDTO> partialUpdateMsttblock(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MsttblockDTO msttblockDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Msttblock partially : {}, {}", id, msttblockDTO);
        if (msttblockDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, msttblockDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!msttblockRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MsttblockDTO> result = msttblockService.partialUpdate(msttblockDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, msttblockDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /msttblocks} : get all the msttblocks.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of msttblocks in body.
     */
    @GetMapping("/msttblocks")
    public ResponseEntity<List<MsttblockDTO>> getAllMsttblocks(Pageable pageable) {
        log.debug("REST request to get a page of Msttblocks");
        Page<MsttblockDTO> page = msttblockService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /msttblocks/:id} : get the "id" msttblock.
     *
     * @param id the id of the msttblockDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the msttblockDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/msttblocks/{id}")
    public ResponseEntity<MsttblockDTO> getMsttblock(@PathVariable Long id) {
        log.debug("REST request to get Msttblock : {}", id);
        Optional<MsttblockDTO> msttblockDTO = msttblockService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msttblockDTO);
    }

    /**
     * {@code DELETE  /msttblocks/:id} : delete the "id" msttblock.
     *
     * @param id the id of the msttblockDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/msttblocks/{id}")
    public ResponseEntity<Void> deleteMsttblock(@PathVariable Long id) {
        log.debug("REST request to delete Msttblock : {}", id);
        msttblockService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
