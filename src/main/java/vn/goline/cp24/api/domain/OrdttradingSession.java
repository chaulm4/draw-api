package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The OrdttradingSession entity.\n@author A true hipster
 */
@Entity
@Table(name = "ordttrading_session")
public class OrdttradingSession implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 10)
    @Column(name = "trading_session_id", length = 10, nullable = false)
    private String tradingSessionId;

    @Size(max = 20)
    @Column(name = "trading_session_sub_id", length = 20)
    private String tradingSessionSubId;

    @Column(name = "trad_date")
    private Integer tradDate;

    @NotNull
    @Size(max = 20)
    @Column(name = "session_cd", length = 20, nullable = false)
    private String sessionCd;

    @Size(max = 20)
    @Column(name = "status", length = 20)
    private String status;

    @Column(name = "trading_flag")
    private Integer tradingFlag;

    @Column(name = "reg_date_time")
    private Instant regDateTime;

    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30)
    private String regUserId;

    @Column(name = "upd_date_time")
    private Instant updDateTime;

    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getTradingSessionId() {
        return this.tradingSessionId;
    }

    public OrdttradingSession tradingSessionId(String tradingSessionId) {
        this.setTradingSessionId(tradingSessionId);
        return this;
    }

    public void setTradingSessionId(String tradingSessionId) {
        this.tradingSessionId = tradingSessionId;
    }

    public String getTradingSessionSubId() {
        return this.tradingSessionSubId;
    }

    public OrdttradingSession tradingSessionSubId(String tradingSessionSubId) {
        this.setTradingSessionSubId(tradingSessionSubId);
        return this;
    }

    public void setTradingSessionSubId(String tradingSessionSubId) {
        this.tradingSessionSubId = tradingSessionSubId;
    }

    public Integer getTradDate() {
        return this.tradDate;
    }

    public OrdttradingSession tradDate(Integer tradDate) {
        this.setTradDate(tradDate);
        return this;
    }

    public void setTradDate(Integer tradDate) {
        this.tradDate = tradDate;
    }

    public String getSessionCd() {
        return this.sessionCd;
    }

    public OrdttradingSession sessionCd(String sessionCd) {
        this.setSessionCd(sessionCd);
        return this;
    }

    public void setSessionCd(String sessionCd) {
        this.sessionCd = sessionCd;
    }

    public String getStatus() {
        return this.status;
    }

    public OrdttradingSession status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTradingFlag() {
        return this.tradingFlag;
    }

    public OrdttradingSession tradingFlag(Integer tradingFlag) {
        this.setTradingFlag(tradingFlag);
        return this;
    }

    public void setTradingFlag(Integer tradingFlag) {
        this.tradingFlag = tradingFlag;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public OrdttradingSession regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public OrdttradingSession regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public OrdttradingSession updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public OrdttradingSession updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrdttradingSession)) {
            return false;
        }
        return tradingSessionId != null && tradingSessionId.equals(((OrdttradingSession) o).tradingSessionId);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrdttradingSession{" +
            "tradingSessionId='" + getTradingSessionId() + "'" +
            ", tradingSessionSubId='" + getTradingSessionSubId() + "'" +
            ", tradDate=" + getTradDate() +
            ", sessionCd='" + getSessionCd() + "'" +
            ", status='" + getStatus() + "'" +
            ", tradingFlag=" + getTradingFlag() +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
