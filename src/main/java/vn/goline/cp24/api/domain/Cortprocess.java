package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The Cortprocess entity.\n@author A true hipster
 */
@Entity
@Table(name = "cortprocess")
public class Cortprocess implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * fieldName
     */
    @NotNull
    @Size(max = 50)
    @Column(name = "process_cd", length = 50, nullable = false)
    private String processCd;

    @Size(max = 50)
    @Column(name = "ref_process_cd", length = 50)
    private String refProcessCd;

    @NotNull
    @Column(name = "jhi_type", nullable = false)
    private Integer type;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "last_time")
    private Instant lastTime;

    @Size(max = 50)
    @Column(name = "last_id", length = 50)
    private String lastId;

    @Column(name = "allow_flag")
    private Integer allowFlag;

    @Column(name = "counter")
    private Integer counter;

    @NotNull
    @Column(name = "status", nullable = false)
    private Integer status;

    @NotNull
    @Column(name = "reg_date_time", nullable = false)
    private Instant regDateTime;

    @NotNull
    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30, nullable = false)
    private String regUserId;

    @NotNull
    @Column(name = "upd_date_time", nullable = false)
    private Instant updDateTime;

    @NotNull
    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30, nullable = false)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getProcessCd() {
        return this.processCd;
    }

    public Cortprocess processCd(String processCd) {
        this.setProcessCd(processCd);
        return this;
    }

    public void setProcessCd(String processCd) {
        this.processCd = processCd;
    }

    public String getRefProcessCd() {
        return this.refProcessCd;
    }

    public Cortprocess refProcessCd(String refProcessCd) {
        this.setRefProcessCd(refProcessCd);
        return this;
    }

    public void setRefProcessCd(String refProcessCd) {
        this.refProcessCd = refProcessCd;
    }

    public Integer getType() {
        return this.type;
    }

    public Cortprocess type(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public Cortprocess name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getLastTime() {
        return this.lastTime;
    }

    public Cortprocess lastTime(Instant lastTime) {
        this.setLastTime(lastTime);
        return this;
    }

    public void setLastTime(Instant lastTime) {
        this.lastTime = lastTime;
    }

    public String getLastId() {
        return this.lastId;
    }

    public Cortprocess lastId(String lastId) {
        this.setLastId(lastId);
        return this;
    }

    public void setLastId(String lastId) {
        this.lastId = lastId;
    }

    public Integer getAllowFlag() {
        return this.allowFlag;
    }

    public Cortprocess allowFlag(Integer allowFlag) {
        this.setAllowFlag(allowFlag);
        return this;
    }

    public void setAllowFlag(Integer allowFlag) {
        this.allowFlag = allowFlag;
    }

    public Integer getCounter() {
        return this.counter;
    }

    public Cortprocess counter(Integer counter) {
        this.setCounter(counter);
        return this;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Cortprocess status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public Cortprocess regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public Cortprocess regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public Cortprocess updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public Cortprocess updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cortprocess)) {
            return false;
        }
        return processCd != null && processCd.equals(((Cortprocess) o).processCd);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cortprocess{" +
            "processCd='" + getProcessCd() + "'" +
            ", refProcessCd='" + getRefProcessCd() + "'" +
            ", type=" + getType() +
            ", name='" + getName() + "'" +
            ", lastTime='" + getLastTime() + "'" +
            ", lastId='" + getLastId() + "'" +
            ", allowFlag=" + getAllowFlag() +
            ", counter=" + getCounter() +
            ", status=" + getStatus() +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
