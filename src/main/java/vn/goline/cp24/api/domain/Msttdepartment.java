package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The Msttdepartment entity.\n@author A true hipster
 */
@Entity
@Table(name = "msttdepartment")
public class Msttdepartment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "department_cd", length = 30, nullable = false)
    private String departmentCd;

    @NotNull
    @Size(max = 100)
    @Column(name = "department_name", length = 100, nullable = false)
    private String departmentName;

    @NotNull
    @Size(max = 30)
    @Column(name = "broker_cd", length = 30, nullable = false)
    private String brokerCd;

    @Size(max = 30)
    @Column(name = "branch_cd", length = 30)
    private String branchCd;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "remarks", length = 300, nullable = false)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @Column(name = "reg_date_time", nullable = false)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30, nullable = false)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @Column(name = "upd_date_time", nullable = false)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30, nullable = false)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Msttdepartment id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentCd() {
        return this.departmentCd;
    }

    public Msttdepartment departmentCd(String departmentCd) {
        this.setDepartmentCd(departmentCd);
        return this;
    }

    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    public String getDepartmentName() {
        return this.departmentName;
    }

    public Msttdepartment departmentName(String departmentName) {
        this.setDepartmentName(departmentName);
        return this;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getBrokerCd() {
        return this.brokerCd;
    }

    public Msttdepartment brokerCd(String brokerCd) {
        this.setBrokerCd(brokerCd);
        return this;
    }

    public void setBrokerCd(String brokerCd) {
        this.brokerCd = brokerCd;
    }

    public String getBranchCd() {
        return this.branchCd;
    }

    public Msttdepartment branchCd(String branchCd) {
        this.setBranchCd(branchCd);
        return this;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public Msttdepartment remarks(String remarks) {
        this.setRemarks(remarks);
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public Msttdepartment regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public Msttdepartment regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public Msttdepartment updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public Msttdepartment updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Msttdepartment)) {
            return false;
        }
        return id != null && id.equals(((Msttdepartment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Msttdepartment{" +
            "id=" + getId() +
            ", departmentCd='" + getDepartmentCd() + "'" +
            ", departmentName='" + getDepartmentName() + "'" +
            ", brokerCd='" + getBrokerCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
