package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The Msttbroker entity.\n@author A true hipster
 */
@Entity
@Table(name = "msttbroker")
public class Msttbroker implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "broker_cd", length = 30, nullable = false)
    private String brokerCd;

    @NotNull
    @Size(max = 100)
    @Column(name = "broker_name", length = 100, nullable = false)
    private String brokerName;

    @Size(max = 100)
    @Column(name = "position", length = 100)
    private String position;

    @Size(max = 100)
    @Column(name = "management_id", length = 100)
    private String managementId;

    @Size(max = 30)
    @Column(name = "department_cd", length = 30)
    private String departmentCd;

    @Size(max = 30)
    @Column(name = "branch_cd", length = 30)
    private String branchCd;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "remarks", length = 300, nullable = false)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @Column(name = "reg_date_time", nullable = false)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30, nullable = false)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @Column(name = "upd_date_time", nullable = false)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30, nullable = false)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Msttbroker id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrokerCd() {
        return this.brokerCd;
    }

    public Msttbroker brokerCd(String brokerCd) {
        this.setBrokerCd(brokerCd);
        return this;
    }

    public void setBrokerCd(String brokerCd) {
        this.brokerCd = brokerCd;
    }

    public String getBrokerName() {
        return this.brokerName;
    }

    public Msttbroker brokerName(String brokerName) {
        this.setBrokerName(brokerName);
        return this;
    }

    public void setBrokerName(String brokerName) {
        this.brokerName = brokerName;
    }

    public String getPosition() {
        return this.position;
    }

    public Msttbroker position(String position) {
        this.setPosition(position);
        return this;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getManagementId() {
        return this.managementId;
    }

    public Msttbroker managementId(String managementId) {
        this.setManagementId(managementId);
        return this;
    }

    public void setManagementId(String managementId) {
        this.managementId = managementId;
    }

    public String getDepartmentCd() {
        return this.departmentCd;
    }

    public Msttbroker departmentCd(String departmentCd) {
        this.setDepartmentCd(departmentCd);
        return this;
    }

    public void setDepartmentCd(String departmentCd) {
        this.departmentCd = departmentCd;
    }

    public String getBranchCd() {
        return this.branchCd;
    }

    public Msttbroker branchCd(String branchCd) {
        this.setBranchCd(branchCd);
        return this;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public Msttbroker remarks(String remarks) {
        this.setRemarks(remarks);
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public Msttbroker regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public Msttbroker regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public Msttbroker updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public Msttbroker updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Msttbroker)) {
            return false;
        }
        return id != null && id.equals(((Msttbroker) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Msttbroker{" +
            "id=" + getId() +
            ", brokerCd='" + getBrokerCd() + "'" +
            ", brokerName='" + getBrokerName() + "'" +
            ", position='" + getPosition() + "'" +
            ", managementId='" + getManagementId() + "'" +
            ", departmentCd='" + getDepartmentCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
