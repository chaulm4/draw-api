package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The Msttblock entity.\n@author A true hipster
 */
@Entity
@Table(name = "msttblock")
public class Msttblock implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "block_cd", length = 30, nullable = false, unique = true)
    private String blockCd;

    @NotNull
    @Size(max = 100)
    @Column(name = "block_name", length = 100, nullable = false)
    private String blockName;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "remarks", length = 300, nullable = false)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @Column(name = "reg_date_time", nullable = false)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30, nullable = false)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @Column(name = "upd_date_time", nullable = false)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30, nullable = false)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Msttblock id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBlockCd() {
        return this.blockCd;
    }

    public Msttblock blockCd(String blockCd) {
        this.setBlockCd(blockCd);
        return this;
    }

    public void setBlockCd(String blockCd) {
        this.blockCd = blockCd;
    }

    public String getBlockName() {
        return this.blockName;
    }

    public Msttblock blockName(String blockName) {
        this.setBlockName(blockName);
        return this;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public Msttblock remarks(String remarks) {
        this.setRemarks(remarks);
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public Msttblock regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public Msttblock regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public Msttblock updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public Msttblock updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Msttblock)) {
            return false;
        }
        return id != null && id.equals(((Msttblock) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Msttblock{" +
            "id=" + getId() +
            ", blockCd='" + getBlockCd() + "'" +
            ", blockName='" + getBlockName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
