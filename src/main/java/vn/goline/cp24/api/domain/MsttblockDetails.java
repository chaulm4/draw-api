package vn.goline.cp24.api.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * The MsttblockDetail entity.\n@author A true hipster
 */
@Entity
@Table(name = "msttblock_details")
public class MsttblockDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Max(value = 8L)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Max(value = 8L)
    @Column(name = "ref_id")
    private Long refId;

    @NotNull
    @Size(max = 30)
    @Column(name = "block_cd", length = 30, nullable = false)
    private String blockCd;

    @NotNull
    @Size(max = 30)
    @Column(name = "branch_cd", length = 30, nullable = false)
    private String branchCd;

    @NotNull
    @Max(value = 1)
    @Column(name = "status", nullable = false)
    private Integer status;

    /**
     * remarks
     */
    @NotNull
    @Size(max = 300)
    @Column(name = "remarks", length = 300, nullable = false)
    private String remarks;

    /**
     * regDateTime
     */
    @NotNull
    @Column(name = "reg_date_time", nullable = false)
    private Instant regDateTime;

    /**
     * regUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "reg_user_id", length = 30, nullable = false)
    private String regUserId;

    /**
     * updDateTime
     */
    @NotNull
    @Column(name = "upd_date_time", nullable = false)
    private Instant updDateTime;

    /**
     * updUserId
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "upd_user_id", length = 30, nullable = false)
    private String updUserId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MsttblockDetails id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRefId() {
        return this.refId;
    }

    public MsttblockDetails refId(Long refId) {
        this.setRefId(refId);
        return this;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public String getBlockCd() {
        return this.blockCd;
    }

    public MsttblockDetails blockCd(String blockCd) {
        this.setBlockCd(blockCd);
        return this;
    }

    public void setBlockCd(String blockCd) {
        this.blockCd = blockCd;
    }

    public String getBranchCd() {
        return this.branchCd;
    }

    public MsttblockDetails branchCd(String branchCd) {
        this.setBranchCd(branchCd);
        return this;
    }

    public void setBranchCd(String branchCd) {
        this.branchCd = branchCd;
    }

    public Integer getStatus() {
        return this.status;
    }

    public MsttblockDetails status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public MsttblockDetails remarks(String remarks) {
        this.setRemarks(remarks);
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Instant getRegDateTime() {
        return this.regDateTime;
    }

    public MsttblockDetails regDateTime(Instant regDateTime) {
        this.setRegDateTime(regDateTime);
        return this;
    }

    public void setRegDateTime(Instant regDateTime) {
        this.regDateTime = regDateTime;
    }

    public String getRegUserId() {
        return this.regUserId;
    }

    public MsttblockDetails regUserId(String regUserId) {
        this.setRegUserId(regUserId);
        return this;
    }

    public void setRegUserId(String regUserId) {
        this.regUserId = regUserId;
    }

    public Instant getUpdDateTime() {
        return this.updDateTime;
    }

    public MsttblockDetails updDateTime(Instant updDateTime) {
        this.setUpdDateTime(updDateTime);
        return this;
    }

    public void setUpdDateTime(Instant updDateTime) {
        this.updDateTime = updDateTime;
    }

    public String getUpdUserId() {
        return this.updUserId;
    }

    public MsttblockDetails updUserId(String updUserId) {
        this.setUpdUserId(updUserId);
        return this;
    }

    public void setUpdUserId(String updUserId) {
        this.updUserId = updUserId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MsttblockDetails)) {
            return false;
        }
        return id != null && id.equals(((MsttblockDetails) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MsttblockDetails{" +
            "id=" + getId() +
            ", refId=" + getRefId() +
            ", blockCd='" + getBlockCd() + "'" +
            ", branchCd='" + getBranchCd() + "'" +
            ", status=" + getStatus() +
            ", remarks='" + getRemarks() + "'" +
            ", regDateTime='" + getRegDateTime() + "'" +
            ", regUserId='" + getRegUserId() + "'" +
            ", updDateTime='" + getUpdDateTime() + "'" +
            ", updUserId='" + getUpdUserId() + "'" +
            "}";
    }
}
