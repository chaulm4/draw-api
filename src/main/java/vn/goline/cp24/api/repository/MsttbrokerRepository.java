package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.Msttbroker;

/**
 * Spring Data SQL repository for the Msttbroker entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsttbrokerRepository extends JpaRepository<Msttbroker, Long> {}
