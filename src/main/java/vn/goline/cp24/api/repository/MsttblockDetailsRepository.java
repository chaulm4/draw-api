package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.MsttblockDetails;

/**
 * Spring Data SQL repository for the MsttblockDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsttblockDetailsRepository extends JpaRepository<MsttblockDetails, Long> {}
