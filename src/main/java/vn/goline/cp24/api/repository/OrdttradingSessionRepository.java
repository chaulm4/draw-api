package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.OrdttradingSession;

/**
 * Spring Data SQL repository for the OrdttradingSession entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrdttradingSessionRepository extends JpaRepository<OrdttradingSession, String> {}
