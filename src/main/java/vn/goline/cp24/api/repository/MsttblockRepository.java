package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.Msttblock;

/**
 * Spring Data SQL repository for the Msttblock entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsttblockRepository extends JpaRepository<Msttblock, Long> {}
