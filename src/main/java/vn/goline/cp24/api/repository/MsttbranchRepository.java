package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.Msttbranch;

/**
 * Spring Data SQL repository for the Msttbranch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsttbranchRepository extends JpaRepository<Msttbranch, Long> {}
