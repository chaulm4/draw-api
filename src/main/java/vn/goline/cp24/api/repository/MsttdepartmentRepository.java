package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.Msttdepartment;

/**
 * Spring Data SQL repository for the Msttdepartment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsttdepartmentRepository extends JpaRepository<Msttdepartment, Long> {}
