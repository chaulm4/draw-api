package vn.goline.cp24.api.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import vn.goline.cp24.api.domain.Cortprocess;

/**
 * Spring Data SQL repository for the Cortprocess entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CortprocessRepository extends JpaRepository<Cortprocess, String> {}
